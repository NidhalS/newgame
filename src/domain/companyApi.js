import api from '../../app/shared/constants/api'
import axios from 'axios'

/**
 * Get all positions of the company. Used to know what are the positions and their ip white list.
 * GET
 */
const getAllPositionsInfo = async () => {
  let response
  try {
    response = await axios.get(`${api.url}/position`, {
      headers: {
        Authorization: `Bearer ${window.localStorage.satellizer_token}`,
      },
    })
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Get all candidates who have access to a company session.
 * It uses the companyStaff info to find the data
 * GET
 */
const getSessionData = async () => {
  let response
  try {
    response = await axios.get(`${api.url}/position/getAllSessionPositions`, {
      headers: {
        Authorization: `Bearer ${window.localStorage.satellizer_token}`,
      },
    })
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Add Candidate and give access to a session.
 * POST
 * @param {Object} data candidate is an Object containing the data of the candidate
 */
const addCandidateSession = async ({ candidate, stepId, companyId }) => {
  let response
  try {
    response = await axios.post(
      `${api.url}/company/createCandidate`,
      {
        firstName: candidate.firstName,
        lastName: candidate.lastName,
        specialId: candidate.specialId,
        sessionDate: candidate.sessionDate,
        positionId: candidate.positionId,
        stepId: stepId,
        companyId: companyId,
      },
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Check if the specialId submited is already used. If the candidate exists, it returns his information.
 * GET
 * @param {String} data specialId to check
 */
const checkCandidate = async ({ specialId }) => {
  let response
  try {
    response = await axios.get(
      `${api.url}/company/checkCandidate/${specialId}`,
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Update Candidate
 * PUT
 * @param {Object} data candidate is an Object containing the data of the candidate
 */
const updateCandidateSession = async ({ candidate }) => {
  let response
  try {
    response = await axios.put(
      `${api.url}/company/updateCandidate`,
      {
        candidateId: candidate._id,
        firstName: candidate.firstName,
        lastName: candidate.lastName,
        specialId: candidate.specialId,
        sessionDate: candidate.sessionDate,
      },
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Delete Candidate.
 * PUT
 * @param {Object} data candidates is an array of candidates
 */
const deleteCandidatesSession = async ({ candidates }) => {
  let response
  try {
    response = await axios.put(
      `${api.url}/company/deleteCandidates`,
      {
        candidates,
      },
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Add ip to limit position access.
 * PUT
 * @param {Object} data ip and id of the position to protect
 */
const addIpWhiteList = async ({ ip, positionId }) => {
  let response
  try {
    response = await axios.put(
      `${api.url}/company/addIp`,
      {
        ip,
        positionId,
      },
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

/**
 * Remove ip restriction.
 * PUT
 * @param {Object} data ip and id of the position
 */
const deleteIpWhiteList = async ({ ip, positionId }) => {
  let response
  try {
    response = await axios.put(
      `${api.url}/company/removeIp`,
      { ip, positionId },
      {
        headers: {
          Authorization: `Bearer ${window.localStorage.satellizer_token}`,
        },
      }
    )
  } catch (error) {
    response = error
  }

  return response
}

export default {
  getAllPositionsInfo,
  getSessionData,
  addCandidateSession,
  checkCandidate,
  updateCandidateSession,
  deleteCandidatesSession,
  addIpWhiteList,
  deleteIpWhiteList,
}
