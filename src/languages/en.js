export default {
  'OFFER_LIST': 'Position List',
  'NOT_YET_PUBLISHED': 'Not yet published',

  'JOB_TITLE': 'Job title',
  'JOB_DESCRIPTION': 'Job description',
  'JOB_OFFER': 'Position',
  'LIST_OF_CANDIDATES': 'List of candidates',
  'SETTINGS': 'Settings',

  'EMAIL': 'Please enter your e-mail address',
  'PASSWORD': 'Password',
  'CONTINUE': 'Continue',
  'CREATE_ACCOUNT': 'Create an account',
  'LINKEDIN_CONNECTION': 'Connect with Linkedin',
  'FACEBOOK_CONNECTION': 'Connect with Facebook',
  'GOOGLE_CONNECTION': 'Connect with Google',
  'GITHUB_CONNECTION': 'Connect with Github',

  // candidate Indentity && position details && survey
  'PROFIL_PICTURE': 'Profile picture',
  'SELECT_PICTURE': 'Select a picture',
  'JOB_DETAILS': 'Position details',
  'COMPANY': 'Company',
  'IDENTITY': 'Identity',
  'CANDIDATES': 'Candidates',
  'CANDIDATE_NAME': 'Last Name',
  'CANDIDATE_FIRSTNAME': 'First Name',
  'CANDIDATE_SPECIAL_ID': 'Special Id',
  'CANDIDATE_MAIL': 'Email',
  'CANDIDATE_COUNTRY': 'Country',
  'CANDIDATE_ADDRESS': 'Address',
  'CANDIDATE_TELEPHONE': 'Telephone',
  'CANDIDATE_CITY': 'Town',
  'CANDIDATE_ZIP': 'ZIP Code',
  'SCHOOL_AND_EXPERIENCES': 'School and experiences',
  'MAIN_SCHOOL': 'Main School',
  'YEAR_EXPERIENCE': 'Years of experience in the job',
  'TOTAL_YEAR_EXPERIENCE': 'Total years of experience',
  'SCHOOL': 'School',
  'NO_CANDIDATE_YET': 'None have replied...',
  'COPIED': 'Copied!',
  'NOSTATUS': 'No status',

  'VALIDATE': 'Validate',
  'PUBLISHED_ON': 'published on ',
  'NOT_PUBLISHED': 'not yet published',
  'SELECT_ALL': 'Select all',
  'FILTER_ARCHIVED': 'With archived candidates',
  'FILTER_NOT_APPLIED': 'With no score candidates',
  'NEXT_STEP': 'Send to next step',
  'SEND_MESSAGE': 'Send Message',
  'ARCHIVE': 'Archive',
  'REFUSE': 'Refuse',
  'ACTIONS': 'Actions',
  'START_DATE': 'Start date',
  'END_DATE': 'End Date',
  'POSITION_TYPE': 'Position type',
  'SKILLS': 'Skill scales',
  'DELETE': 'Delete',
  'EDIT': 'Modify',
  'EDIT_STEPS': 'Modify Steps',
  'SEND': 'Send',
  'TEMPLATE': 'Template',
  'CREATE_TEMPLATE': 'Add template',
  'NEW_TEMPLATE': 'New template',
  'TEMPLATE_NAME': 'Template name',
  'TEMPLATE_PARAM': 'Template settings',
  'DELETE_TEMPLATE_TITLE': 'Delete template',
  'DELETE_TEMPLATE': 'Are you sure to delete this template?',

  // Special skills
  'IMPORT_CV': 'Please import your CV',
  'IMPORT_PORTFOLIO': 'Please import your Portfolio',
  'FILE_SELECTED': 'File selected :',
  'INSTRUCTION_DRAG_CV': 'Drag and drop your file here in word or pdf format',
  'INSTRUCTION_REPLACE_CV': 'Drag and drop your file to replace the current file',
  'INSTRUCTION_DRAG_PORTFOLIO': 'Drag and drop your file here in pdf, jpg or png format',
  'INSTRUCTION_REPLACE_PORTFOLIO': 'Drag and drop your file to replace the current file',
  'AVAILABILITY_DATE': 'What is your availability start?',
  'ADD_FILE': 'Add a file',
  'LENGTH': 'Length',
  'AVAILABILITY': 'Availability',
  'BIRTH_DATE': 'Birth date',

  // APPLY CANDIDATE
  'LEGAL_NOTES': 'Legal notes',
  'ERROR_FILE_CV': 'The file must be in word or pdf format',
  'ERROR_FILE_PORTFOLIO': 'The file must be in pdf, jpg or png format',
  'ERROR_FILE_CUSTOM': 'The file must be in pdf, jpg, png, doc or docx format',
  'VALIDATE_APPLICATION': 'Do you want to validate the form ?',
  'YES': 'Yes',
  'NO': 'No',
  'FIRSTNAME': 'First Name',
  'LASTNAME': 'Last Name',
  'PHONE': 'Telephone',
  'EMAIL_NOT_VALID': 'Your email is not a valid email',
  'INCOMPLETE_FIELDS': 'You did not fill the following fields : ',
  'OK': 'Ok',
  'APPLICATION_SENT_TITLE': 'Your application has been sent !',
  'VALIDATE_STAR' : 'Sorry, you must accept the gray statement to continue, it is mandatory to start the game.',
  'QUIT_APPLICATION' : 'cancel',


  // Candidate position detail
  'APPLICATION_SENT_TEXT': 'We are going to process it and we will send you an answer soon !',
  'INFO_UPDATED': 'Informations have been updated',
  'CLOSE': 'Close',
  'CLOSE_FEED':'Close',

  // Candidate detail
  'ACCOUNT_CHECKED' : 'checked account',
  'SEND_TO': 'Send ',
  'TO_NEXT_STEP': 'to next step ?',
  'SEND_EMAIL_REFUSE': 'Send an refuse email to ',
  'CANCEL': 'Cancel',
  'DOWNLOAD_SCORE_PDF' : 'Download results in pdf',
  'MAKE_PASS_AGAIN': 'Let retake a test',
  'ADD_STATUS':'Add status',
  'EDIT_STATUS':'Update status',
  'ERROR_CHANGE_STATUS': 'You can not change this status',
  'ERROR':'Error',




  // SKill Dialog
  'NAME_SKILL': 'Name of the skill',
  'QUESTION_SKILL': 'Question on the skill',
  'ADD_SKILLS': 'Add skills',
  'SEARCH_SKILL' : 'Search Skill',

  // Position metrics
  'RESPONSE_RATE': 'Response rate',

  // Position detail component
  'TARGET_CANDIDATE': 'Ideal candidate',
  'NUMBER_CANDIDATES': 'nbre candidates',
  'FULL_APPLY_METRIC': 'Full application',
  'LOGIN_METRIC': 'Login',
  'APPLY_METRIC': 'Arrived on site',

  // Edit step skill special
  'SPECIAL_FIELDS': 'Specific questions',

  // positionInfo
  'IN_PROGRESS': 'in progress',
  'ARCHIVED': 'archived',
  'DELETED': 'deleted',
  'OVER': 'out of',
  'ANONYMOUS': 'Anonymous',

  // actionButton
  'SUGGEST_JOB': 'suggest a job',

  // candidateList
  'DOWNLOAD_PORTFOLIO': 'Download Portfolio',
  'NO_PORTFOLIO': 'No portfolio',
  'DOWNLOAD_FILE': 'Download File',
  'NO_FILE': 'No file',


  // vivier
  'RESULTS': 'results',
  'SORT_BY_DATE': 'Sort by date',
  'SEARCH_FILTER': 'Email, first name, last name',
  'KEYWORDS': 'Key words',
  'NO_SORT': 'Sort by score',
  'LATER_FIRST' : 'Oldest first',
  'SOONER_FIRST' : 'Newest first',

  // CandidateIdentityinfo
  'REQUIRED': 'Required',
  'EMAIL_EXISTS': 'This email is already used, please use another one or connect to the other account',

  // customEvent
  'ADD_ACTION_TIMELINE': 'Add an action to the timeline',
  'COLOR': 'Color',
  'LABELS': 'Labels',
  'YOUR_ACTION': 'Your action',
  'TRAINING': 'Training',
  'NEW_ROUND' : 'New Round',

  // SkillSpecialDialog
  'ADD_SPECIAL_QUESTION': 'Add a special question',

  'STEP': 'Step ',

  // clock
  'HOURS': 'Hours',
  'MINUTES': 'Minutes',
  'SECONDS': 'Seconds',

  // Portfolio
  'PORTFOLIO': 'Portfolio',

  // login.hmtl
  'ERROR_EMAIL': 'Please insert an email',
  'ERROR_PASSWORD': 'Please insert a password',
  'ACCOUNT_DOESNT_EXIST': 'This account doesn\'t exist',
  'ALREADY_HAVE_ACCOUNT_YOU_WILL_RECEIVE_MAIL_WITH_PWD': 'You already have an account with this email, do you want us to send you an email with the password ?',
  'CANNOT_CONNECT': 'Connection is not available',
  'ACCEPT_EVALUATION' : 'Evaluate your application for this position ?',
  'ACCEPT_CONTACT' : 'Offer you other opportunities ?',
  'COOKIES_TEXT' : 'We use cookies to save your progress and make your navigation easier.',
  'ACCEPT_USE' : 'Do you allow the recruiter to use your data to',
  'ACCEPT_LEGAL_NOTICE': 'You have to accept the legal notice',
  'POPUP_BROWSER' : 'Your browser does not allow you to have an optimal experience for this online path. We advise you to use Google Chrome, Safari or Firefox.',
  'POPUP_DEVICE' : 'Your device does not provide an optimal experience for this online path. We advise you to use a smartphone with a minimum size of 4.7 inches (corresponding to an Iphone 7) or a computer.',
  'WARNING' : 'Warning',
  // position creation form
  'COUNTRY': 'Country',
  'ADDRESS': 'Address',
  'TELEPHONE': 'Telephone',
  'CITY': 'Town',
  'ZIP': 'ZIP code',

  'COMPULSORY_FIELD': 'This field is compulsory',

  // panelHeader
  'IMPORTANT': 'Important',
  'SECONDARY': 'Desirable',
  'OPTION': 'Option',
  'TOP': 'Top',
  'BOTTOM': 'Bottom',
  'BEFORE': 'Before',
  'AFTER': 'After',
  'BEFORE_LONG_TITLE': 'Preliminary questions',
  'AFTER_LONG_TITLE': 'Final questions',
  'OPTION_LONG_TITLE': 'Optional',

  // Nav
  
  'CANDIDATE_SPACE': 'I am a Candidate',
  'RECRUITER_SPACE': 'Recruiter\'s login',
  'POSITIONS': 'Positions',
  'CANDIDATE_PROFILE': 'My info',
  'SKILLS_MENU': 'Skills',
  'CANDIDATES_LIST': 'Candidates',
  'CANDIDATE_POOL': 'Candidate Pool',
  'SURVEYS': 'Surveys',
  'ADMIN': 'Admin',

  // ButtonParam
  'DISCONNECT': 'Disconnect',
  'PARAMETERS': 'Parameters',
  'FRENCH': 'French',
  'ENGLISH': 'English',
  'CHINESE': 'Chinese',
  'ARABIC': 'Arabe',
  'ACCOUNT_INFO': 'Account info',

  // CandidateForm index
  'SIGN_IN': 'Sign In',
  'SIGN_UP': 'Sign Up',
  'SIGN_WITH': 'Connect with',
  'I_FORGOT_MY_PASSWORD': 'I forgot my password',
  'SIGN_IN_OR_UP': 'Type your email to sign in or create an account',

  // applyPosition
  'PREVIOUS': 'Previous',
  'NEXT': 'Next',
  'DONE': 'Done',
  'I_ACCEPT_THE': 'I accept the',
  'POSITION_NOT_FOUND': 'Oops the position does not exist',
  'APPLY': 'START',
  'BEGIN' : 'START',

  // login company
  'ERROR_MATCHING_EMAIL_PASSWORD': 'The email and password don\'t match',
  'ERROR_MAX_ATTEMPTS_REACHED': 'Too many requests have been made, please wait',

  'ACCOUNT_ALREADY_EXISTS': 'The account already exists',

  'YOU_WILL_APPLY_TO_STEP': 'You\'re going to apply to step ',
  'IN_A_FEW_CLICKS': ' in a few clicks (1mn)',

  // ScoringPanel
  'DOWNLOAD_CV': 'Download CV',
  'NO_CV': 'No CV',
  'COMPULSORY': 'Compulsory',

  // parameters
  'PASSWORDS_DONT_MATCH': 'Passords don\'t match',
  'CONFIRM_PASSWORD': 'Confirm the password',
  'CHANGE_PASSWORD': 'Change the password',
  'CONFIRM_DELETE_ACCOUNT': 'Do you really want to delete your account ?',
  'WE_SEND_YOU_AN_EMAIL_CONFIRMATION': 'We just sent you an email to delete your application.',
  'TITLE':'Title',
  'ASSOCIED_COLOR':'associed color',
  'ADD_TO_COLLECT':'Add to collect',
  'ICON':'Icon',
  'DELAY_MONTHS': 'Delay (months)',
  'ANONYMIZATION' : 'Anonymization',
  'ANONYMISE' : 'Anonymise',
  'CANDIDATES_ANONYMISED': 'Candidates anonymised',

  // Edit Step
  'CONFIRM_DELETE_STEP': 'Remove this step ?',

  // Edit Position
  'CONFIRM_DELETE_POSITION': 'Remove this position ?',

  // positionlist
  'COPY_URL': 'Copy URL',
  'CV': 'CV',
  'NAME': 'Name',
  'DATE': 'Date',
  'CANDIDATES_FILTER': 'Candidates',

  // candidate form
  'I_ALREADY_HAVE_ACCOUNT': 'I already have an account',

  'CREATE_ACCOUNT_FOR_EMAIL': 'You\'re going to create an account for the email ',
  'EMAIL_SENT': 'An email was sent to your account ',
  'TO_CONNECT': ' to connect.',

  // position creation form
  'INTERNAL_NOTE': 'Internal Note',

  // timeline
  'CANDIDATE_ACCOUNT_CREATED': 'The candidate account is created',
  'CANDIDATE_APPLIED_TO': 'The candidate applied to step',
  'CANDIDATE_START_TIMER': 'The candidate start the case study',
  'SENT_TO': 'sent to',
  'WAS_SENT': 'was sent',
  'TO_STEP': 'to the step',
  'REJECTED_CANDIDATE': 'rejected the application of',
  'FOR_POSITION': 'for the position',
  'ARCHIVED_CANDIDATE': 'archived the application of',
  'CONNECTED_TO_STEP': 'The candidate connected to the step',
  'CANDIDATE_DELETED': 'The candidate deleted his application',
  'WROTE': 'wrote',
  'SEND_PRIVATE_MAIL': 'send a private message',
  'SEND_FEEDBACK_BY_STAFF': 'send a textual feedback',
  'SEND_REDO_STEP_BY_STAFF': 'sent a message to take test(s) again',
  'DEFINE_STATUS':'Define a status',
  'COMMENT':'Comment',


  // CandidatePositionDetail
  'BACK': 'Back',
  'NO_UPDATE_TO_DO': 'There is nothing to update',
  'SCORE': 'Score',
  'PROFILE': 'Profile',
  'GLOBAL_SCORE': 'Global score',
  'DETAIL_GLOBAL_SCORE': 'Details global score',
  'PROGRESS': 'Progress',
  'REST': 'Rest',
  'SEE_DETAIL': 'See details',
  'IN_TIME' : 'Total time',
  'OUT_OF_SCORE' : 'Out of score',
  'PDF_SETTINGS': 'Pdf settings',
  'PDF_ZONE_1': 'Show details of skills and special questions',
  'PDF_ZONE_2': 'Show score per component, and components without score',
  'PDF_ZONE_3': 'Show questionnaries\' details',
  'PDF_ZONE_4': 'Show the details of the questions, without score',
  'PASS_AUTOMATIC': 'Send automatically to the next step',
  'CONFIRMATION_MAIL' : 'We just have sent you an email.',
  'NOTIFY_NEW_CANDIDATES' : 'Notify me when there are new candidates',
  'NOTIFY_WHEN_MAILING_CANDIDATE' : 'Notify me when I send an email to a candidate',

  

  // CandidateSkills
  'NO_SKILL': 'You have no skill registered yet',

  // positionList
  'NO_POSITION': 'You didn\'t apply to any position yet',
  // positionList
  'COMPANY_NO_POSITION': 'You didn\'t create any position',

  // editStep
  'DRAG_SKILL_HERE': 'Drag and drop the skill here',
  'PONDERATION' : 'weighting',

  // Minigame 01
  'INTRO_MINIGAME_01': ' For this test we will measure your impulsivity \n\nClick on the green button only when the image is a heart.',
  'START': 'Start',
  'FINISH': 'Game over',
  'NO_HEART': 'You took no heart',
  'ONE_HEART': 'You took one heart',
  'MANY': 'You took',
  'HEARTS': 'hearts',
  'NO_FISH': 'and no fish',
  'ONE_FISH': 'and one fish',
  'FISHED': 'fishes',
  'ONLY_TOUCH_HEART': 'Only tap if it\'s a heart',

  // questionnaireCreation
  'CHECKBOX': 'Multiple choice',
  'RADIO': 'Single choice',
  'ORDERING': 'Ordering',
  'TEXTAREA': 'Text box',
  'VIDEO': 'Video',
  'IMAGE': 'Image',
  'IMAGEMULTI': 'Image multiple choice',
  'BOARD': 'Board images, text, video',

  'CARD': 'Card',
  'BASIC': 'Basic',
  'CASE_STUDY' : 'Etude de cas',
  'AVAILABLE_DOCUMENTS' :'Available documents',
  'EDIT_POPUP': 'Edit popup',
  'UPLOAD': 'Upload',
  'FINISH_QUIT': 'Finish and Exit',

  // questionnairesList
  'SURVEYS_LIST': 'Surveys List',

  // Edit Questionnaire
  'CONFIRM_DELETE_SURVEY': 'Remove this position ?',

  // email window
  'EMAIL_TITLE': 'Send email',
  'SUBJECT': 'Subject',
  'MESSAGE': 'Message',

  // Questionnaire
  'SEND_QUESTIONNAIRE': 'Continue',
  'QUESTIONNAIRE_NO_NAME': 'Please give a name to the questionnaire',
  'QUESTIONNAIRE_NO_PARTS': 'We cannot create the questionnaire, there is no part created',
  'QUESTIONNAIRE_EMPTY': 'We cannot create the questionnaire, it is empty',
  'NO_QUESTIONNAIRE': 'You didn\'t create any questionnaire',
  'DELETE_COLUMN': 'Do you want to delete this column ?',
  'ADD_ANSWER': 'Please specify',

  // Case study
  'SUBMIT': 'Yes, send',
  'CONFIRM_ANSWERS': 'Are you sure you’ve completed this test?',
  'SEND_ANSWERS' : 'Send your answers and proceed to the next step.',
  'STAY_ON_CASE_STUDY' : 'Back to the case study',
  'NEW_DOCUMENT' : 'New document',
  'TIME_IS_OVER' : 'Time is over, your answers are recorded',

  // Workmemory
  'INTRO_WORKMEMORY': 'In this game, you must remember the letters displayed, then depending on the level of diffilculty, tell if it was the same letter, one, two, or three letters before. Try to answer as fast as you can and make as little mistakes as possible',
  'END_GAME': 'Congratulations, you finished this game.',
  // countdown
  'GO': 'Go',

  // questionnaire service
  'TEXT_BETWEEN': 'The text must be between',
  'AND': 'and',
  'TEXT_LESS_THAN': 'The text must be less than',
  'TEXT_GREATER_THAN': 'The text must be greater than',
  'CHARACTERS': 'characters',
  'TEXT_INPUT': 'Type your text here',


  // Concurrence
  'INTRO_COMPETITION': 'In the following game, you will have to select activities into which you can project yourself.',
  'FREEZE': 'Freeze',
  'UNFREEZE': 'Unfreeze',
  'LIMIT_OF': 'Limit of',
  'KEY_WORDS_LIMIT': 'keywords reached',
  'WORD': 'point',
  'WORDS': 'points',

  // Axes
  'AXES': 'Assessment components',


  // taskSwitching 2
  'ODD': 'Red',
  'EVEN': 'Green',
  'CONSONANT': 'Left',
  'VOWEL': 'Right',

  'PLAY': 'Play',

  // London towers
  'START_TOWER' : 'start',
  'FINISH_TOWER' : 'finish',
  'SKIP_GAME' : 'Do you really want to give up?',

  'GIVE_UP' : 'Give up',


  // minigame end score
  'MINIGAME_END_TEXT_1': 'Congratulations, you succeded in the 2 games! You have the skills to become the perfect Project Manager!',
  'MINIGAME_END_TEXT_2': 'Nice score! You did great in one of the two games (Task Switching) - you have all the skills to become a good Content Developer',
  'MINIGAME_END_TEXT_3': 'Nice score! You did great in one of the two games (Task Switching) - you have all the skills to become a good Technical Writer',
  'MINIGAME_END_TEXT_4': 'Nice! You are quite good in both games! You have the skills to become a Content Developer',

  'YOUR_SCORE_IS': 'Your score is',
  //Balloon game
  'YOU_EARNED' : 'You have collected',
  'TOTAL_GAIN' : 'Total gain',

  //company parameter
  'PASSWORD_CONSTRAINT': 'Your password must contain at least 12 characters, 1 uppercase, 1 number and 1 special character (!@%^&*#\\-_).',
  'OTHER' :'Other',

   //feed back parameters
   'SEND_FEEDBACK' : 'Send feedback',
   'SEND_FEEDBACK_IN_CANDIDATE_LANG' : 'Send feedback',
   'DOWNLOAD_CANDIDATE' : 'Dowload candidate list',
   'SEND_EMAIL_VALIDATION' : 'Request email validation',
   //accountInfo
  'STAFF_MASTER': 'Use the master account settings',
  //PlayfulSpirit
  'I_AM': 'I am very',

  //React Session
  'ADD_CANDIDATE': 'Add a candidate',
  'ROWS_PER_PAGE': 'Rows per page',
  'CANDIDATE_LIST': 'Candidate list',
  'IP_WHITE_LIST': 'Ip list',
  'ADD_IP': 'Add an ip',
  'LOG_IN_TEXT': 'Please log in',
  'LOG_IN_BUTTON': 'Log in',
  'APPLICANT_ID': 'ApplicantId',

  //others
  'COLON': ':',
};
