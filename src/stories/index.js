/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react'
import styled, { createGlobalStyle } from 'styled-components'
import ReactMarkdown from 'react-markdown'
import BackgroundLayout from '../components/BackgroundLayout'
import Card from '../components/Card'
import readme from '../README.md'

const GlobalStyle = createGlobalStyle`
  // Might break on storybook update
  .css-1enb6fx > * {
    width: 100%;
    margin: 0;
  }
`

const Wrapper = styled(Card)`
  width: 80%;
  margin: 40px auto;
  padding: 0 30px 30px;
  text-align: left;

  /* Markdown styling  */
  h1,
  h2,
  h3 {
    margin: 24px 0 16px;
  }

  h1,
  h2 {
    padding-bottom: 4px;
    border-bottom: 2px solid #58382c;
  }

  p {
    margin-bottom: 16px;
  }

  li {
    margin-bottom: 8px;
  }

  a {
    color: white;
  }

  pre {
    overflow: auto;
  }
`

const Intro = () => (
  <BackgroundLayout
    backgroundImg="https://s3-eu-west-1.amazonaws.com/rhtool/image/BG_detective.png"
    backgroundColor="#908787"
  >
    <GlobalStyle />
    <Wrapper>
      <ReactMarkdown source={readme} escapeHtml={false} />
    </Wrapper>
  </BackgroundLayout>
)

export default {
  title: 'Introduction|Storybook',
  parameters: {
    component: Intro,
  },
}

export const main = () => <Intro />
