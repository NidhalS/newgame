import React from 'react'
import { action } from '@storybook/addon-actions'
import logo from '../../Memory/stories/logo.svg'

import GameTutorial from '..'

export default {
  title: 'GameTutorial',
  parameters: {
    component: GameTutorial,
  },
}

export const main = () => (
  <GameTutorial logo={logo} onClick={action('onClick')}>
    GameTutorial
  </GameTutorial>
)
