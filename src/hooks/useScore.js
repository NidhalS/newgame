import { useState } from 'react'

export default ({ step }) => {
  const [score, setScore] = useState(0)

  return {
    score,
    onAnswer: () => {
      setScore((score) => score + step)
    },
  }
}
