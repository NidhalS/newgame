import React from 'react'
import { action } from '@storybook/addon-actions'

import GameEnding from '..'

export default {
  title: 'GameEnding',
  parameters: {
    component: GameEnding,
  },
}

export const main = () => <GameEnding onClick={action('onClick')} />
