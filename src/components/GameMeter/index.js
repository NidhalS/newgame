import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 20px;
  background-color: #fff;
  border-radius: 10px;
  margin-left: 20px;
  margin-right: 20px;
  filter: drop-shadow(3px 3px 3px #555);
  overflow: hidden;
`

const Content = styled.div`
  height: 100%;
  border-radius: 10px;
  background: ${({ type }) => (type === 'good' ? '#5f9ea0' : 'red')};
  transition: 100ms;
`

/**
 * Show a meter, used to track score
 */
const GameMeter = ({ type, value }) => (
  <Wrapper>
    <Content style={{ width: `${value * 100}%` }} type={type} />
  </Wrapper>
)

GameMeter.propTypes = {
  /**
   * Used for style only
   */
  type: PropTypes.oneOf(['good', 'bad']).isRequired,
  /**
   * From 0 to 1
   */
  value: PropTypes.number.isRequired,
}

export default GameMeter
