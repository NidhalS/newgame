import React from 'react'
import { action } from '@storybook/addon-actions'
import MemoryBlock from '../../MemoryBlock'

import data from '../../Memory/stories/data.json'
import logo from '../../Memory/stories/logo.svg'

import Game from '..'

export default {
  title: 'Game',
  parameters: {
    component: Game,
  },
}

export const main = () => (
  <Game
    data={data}
    logo={logo}
    lang="fr"
    onFinishGame={action('onFinishGame')}
    onNextGame={action('onNextGame')}
    Block={MemoryBlock}
  />
)
