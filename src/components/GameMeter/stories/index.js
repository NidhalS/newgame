import React from 'react'
import styled from 'styled-components'
import { withKnobs, number, select } from '@storybook/addon-knobs'

import GameMeter from '..'

const Wrapper = styled.div`
  margin: 40px;
`

export default {
  title: 'GameMeter',
  decorators: [withKnobs],
  parameters: {
    component: GameMeter,
  },
}

const valueOptions = {
  range: true,
  min: 0,
  max: 1,
  step: 0.1,
}

const typeOptions = {
  good: 'good',
  bad: 'bad',
}

export const main = () => (
  <Wrapper>
    <GameMeter
      value={number('value', 0.5, valueOptions)}
      type={select('type', typeOptions, 'good')}
    />
  </Wrapper>
)
export const full = () => (
  <Wrapper>
    <GameMeter value={1} type="good" />
  </Wrapper>
)
export const empty = () => (
  <Wrapper>
    <GameMeter value={0} type="good" />
  </Wrapper>
)
export const bad = () => (
  <Wrapper>
    <GameMeter value={0.5} type="bad" />
  </Wrapper>
)
