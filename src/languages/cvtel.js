export default {
  'OFFER_LIST': 'Liste des métiers CVtel',
  'NOT_YET_PUBLISHED': 'Pas encore publiée',

  'JOB_TITLE': 'Intitulé du poste',
  'JOB_DESCRIPTION': 'Description poste',
  'JOB_OFFER': 'Métier',
  'LIST_OF_CANDIDATES': 'Liste candidats',
  'SETTINGS': 'Paramètres',

  'EMAIL': 'Email',
  'PASSWORD': 'Mot de passe',
  'CONTINUE': 'Continuer',
  'CREATE_ACCOUNT': 'Créer un compte',
  'LINKEDIN_CONNECTION': 'Se connecter avec Linkedin',
  'FACEBOOK_CONNECTION': 'Se connecter avec Facebook',
  'GOOGLE_CONNECTION': 'Se connecter avec Google',
  'GITHUB_CONNECTION': 'Se connecter avec Github',

  // candidate Indentity && position details && survey
  'PROFIL_PICTURE': 'Photo du profil',
  'SELECT_PICTURE': 'Sélectionnez une photo',
  'JOB_DETAILS': 'details du profil',
  'COMPANY': 'Société',
  'IDENTITY': 'Identité',
  'CANDIDATES': 'candidats',
  'CANDIDATE_NAME': 'Nom',
  'CANDIDATE_FIRSTNAME': 'Prénom',
  'CANDIDATE_MAIL': 'Email',
  'CANDIDATE_COUNTRY': 'Pays',
  'CANDIDATE_ADDRESS': 'Adresse',
  'CANDIDATE_TELEPHONE': 'Téléphone',
  'CANDIDATE_CITY': 'Ville',
  'CANDIDATE_ZIP': 'Code postal',
  'SCHOOL_AND_EXPERIENCES': 'Formation et experiences',
  'MAIN_SCHOOL': 'Formation principale',
  'YEAR_EXPERIENCE': 'Années d\'expérience dans le métier',
  'TOTAL_YEAR_EXPERIENCE': 'Années d\'expérience totales',
  'SCHOOL': 'Formation',
  'NO_CANDIDATE_YET': 'Aucun candidat n\'a encore répondu...',
  'COPIED': 'Copié!',

  'VALIDATE': 'Valider',
  'PUBLISHED_ON': 'publiée le ',
  'NOT_PUBLISHED': 'pas encore publiée',
  'SELECT_ALL': 'Tout sélectionner',
  'NEXT_STEP': 'Envoyer à l\'étape suivante',
  'SEND_MESSAGE': 'Envoyer un message',
  'ARCHIVE': 'Archiver',
  'REFUSE': 'Refuser',
  'START_DATE': 'Date début',
  'END_DATE': 'Date fin',
  'POSITION_TYPE': 'Type de poste',
  'SKILLS': 'Compétences',
  'DELETE': 'Supprimer',
  'EDIT': 'Modifier',
  'EDIT_STEPS': 'Modifier Etapes',

  // Special skills
  'IMPORT_CV': 'Veuillez importer votre CV',
  'IMPORT_PORTFOLIO': 'Veuillez importer votre Portfolio',
  'FILE_SELECTED': 'Ficher sélectionné :',
  'INSTRUCTION_DRAG_CV': 'Glisser et déposer un fichier au format word doc, docx ou pdf',
  'INSTRUCTION_REPLACE_CV': 'Drag and droppez pour remplacer le CV enregistré',
  'INSTRUCTION_DRAG_PORTFOLIO': 'Glisser et déposer un fichier au format pdf, jpg ou png',
  'INSTRUCTION_REPLACE_PORTFOLIO': 'Glisser et déposer pour remplacer le Portfolio enregistré',
  'AVAILABILITY_DATE': 'Quelles sont vos dates de disponibilités ?',
  'ADD_FILE': 'Ajouter un fichier',
  'LENGTH': 'Durée du stage (en mois)',
  'AVAILABILITY': 'Disponibilité',

  // APPLY CANDIDATE
  'LEGAL_NOTES': 'mentions légales',
  'ERROR_FILE_CV': 'Le fichier doit être au format doc, docx ou pdf',
  'ERROR_FILE_PORTFOLIO': 'Le fichier doit être au fomat pdf, jpg ou png',
  'VALIDATE_APPLICATION': 'Valider le questionnaire ?',
  'YES': 'Oui',
  'NO': 'Non',
  'FIRSTNAME': 'Prénom',
  'LASTNAME': 'Nom',
  'PHONE': 'Téléphone',
  'EMAIL_NOT_VALID': 'Votre email n\'est pas un email valide',
  'INCOMPLETE_FIELDS': 'Vous n\'avez pas rempli le(s) champ(s) suivant(s): ',
  'OK': 'Ok',
  'APPLICATION_SENT_TITLE': 'Votre candidature a été envoyée !',

  // Candidate position detail
  'APPLICATION_SENT_TEXT': 'Nous allons la traiter et nous vous enverrons une réponse dans les plus brefs délais !',
  'INFO_UPDATED': 'Informations mises à jour',
  'CLOSE': 'Fermer',

  // Candidate detail
  'SEND_TO': 'Faire passer ',
  'TO_NEXT_STEP': 'à l\'étape suivante ?',
  'SEND_EMAIL_REFUSE': 'Envoyer un mail de refus à',
  'CANCEL': 'Annuler',

  // SKill Dialog
  'NAME_SKILL': 'Nom de la compétence',
  'QUESTION_SKILL': 'Question sur la compétence',
  'ADD_SKILLS': 'Ajout compétences',
  'TITLE_CATEGORY': 'Catégorie',

  // Position metrics
  'RESPONSE_RATE': 'Taux de réponse',

  // Position detail component
  'TARGET_CANDIDATE': 'Candidat idéal',
  'NUMBER_CANDIDATES': 'nbre candidats',
  'FULL_APPLY_METRIC': 'Candidature(s) complète(s)',
  'LOGIN_METRIC': 'Login',
  'APPLY_METRIC': 'Arrivé(s) sur le site',

  // Edit step skill special
  'SPECIAL_FIELDS': 'questions spécifiques',

  // positionInfo
  'IN_PROGRESS': 'en cours',
  'ARCHIVED': 'archivé(s)',
  'DELETED': 'supprimé(s)',
  'OVER': 'sur',
  'ANONYMOUS': 'Anonyme',

  // actionButton
  'ACTIONS': 'Actions',
  'SUGGEST_JOB': 'proposer un poste',

  // candidateList
  'SEE_PORTFOLIO': 'Portfolio',

  // vivier
  'RESULTS': 'résultat(s)',
  'SORT_BY_DATE': 'Trier par date',
  'KEYWORDS': 'Mots clefs',

  // CandidateIdentityinfo
  'REQUIRED': 'Obligatoire',
  'EMAIL_EXISTS': 'Cet email existe déjà, merci d\'en utiliser un autre ou de vous connecter sur ce compte',

  // customEvent
  'ADD_ACTION_TIMELINE': 'Ajouter une action à la timeline',
  'COLOR': 'Couleur',
  'YOUR_ACTION': 'Votre action',

  // SkillSpecialDialog
  'ADD_SPECIAL_QUESTION': 'Ajouter une question spéciale',

  'STEP': 'Etape ',

  // clock
  'HOURS': 'Heures',
  'MINUTES': 'Minutes',
  'SECONDS': 'Secondes',

  // Portfolio
  'PORTFOLIO': 'Portfolio',

  // login.hmtl
  'ERROR_EMAIL': 'Merci de rentrer un email valide',
  'ERROR_PASSWORD': 'Merci de rentrer un mot de passe',
  'ACCOUNT_DOESNT_EXIST': 'Ce compte n\'existe pas',

  // position creation form
  'COUNTRY': 'Pays',
  'ADDRESS': 'Adresse',
  'TELEPHONE': 'Téléphone',
  'CITY': 'Ville',
  'ZIP': 'Code postal',
  'COMPULSORY_FIELD': 'Ce champ est obligatoire',

  // panelHeader
  'IMPORTANT': 'Importants',
  'SECONDARY': 'Souhaits',
  'OPTION': 'Options',
  'TOP': 'Haut',
  'BOTTOM': 'Bas',
  'BEFORE': 'Avant',
  'AFTER': 'Après',
  'BEFORE_LONG_TITLE': 'Questions préliminaires',
  'AFTER_LONG_TITLE': 'Questions finales',
  'OPTION_LONG_TITLE': 'Optionnelles',

  // Nav
  'CANDIDATE_SPACE': 'Espace candidats',
  'RECRUITER_SPACE': 'Espace recruteurs',
  'POSITIONS': 'Métiers',
  'CANDIDATE_PROFILE': 'Mes informations',
  'SKILLS_MENU': 'Compétences',
  'CANDIDATES_LIST': 'Profils',
  'CANDIDATE_POOL': 'Vivier',
  'SURVEYS': 'Questionnaires',

  // ButtonParam
  'DISCONNECT': 'Se déconnecter',
  'PARAMETERS': 'Paramètres',
  'FRENCH': 'Français',
  'ENGLISH': 'Anglais',
  'CHINESE': 'Chinois',
  'ARABIC': 'Arabe',

  // sidenavbar
  'MENU': 'Menu',

  // CandidateForm index
  'SIGN_IN': 'Je me connecte',
  'SIGN_UP': 'Je crée mon compte',
  'SIGN_WITH': 'Je me connecte avec',
  'I_FORGOT_MY_PASSWORD': 'J\'ai oublié mon mot de passe',
  'SIGN_IN_OR_UP': 'Entrez votre email pour vous connecter ou créer un compte',

  // applyPosition
  'PREVIOUS': 'Précédent',
  'NEXT': 'Suivant',
  'DONE': 'Fin',
  'I_ACCEPT_THE': 'J\'accepte les',
  'TIME_OUT': 'Il y a un problème de connection, ré-essayez plus tard',
  'POSITION_NOT_FOUND': 'Oops la position n\'existe pas',

  // login company
  'ERROR_MATCHING_EMAIL_PASSWORD': 'l\'email et le mot de passe ne correspondent pas',

  'ACCOUNT_ALREADY_EXISTS': 'Le compte existe déjà',

  'YOU_WILL_APPLY_TO_STEP': 'Vous allez postuler à l\'étape ',
  'IN_A_FEW_CLICKS': ' en quelques clicks (1mn)',

  // ScoringPanel
  'SEE_CV': 'Voir CV',
  'COMPULSORY': 'Obligatoire',

  // parameters
  'PASSWORDS_DONT_MATCH': 'Les mots de passe ne correspondent pas',
  'CONFIRM_PASSWORD': 'Confirmer le mot de passe',
  'CHANGE_PASSWORD': 'Changer le mot de passe',
  'CONFIRM_DELETE_ACCOUNT': 'Voulez-vous vraiment supprimer votre compte ?',

  // Edit Step
  'CONFIRM_DELETE_STEP': 'Voulez-vous vraiment supprimer cette étape ?',

  // Edit Position
  'CONFIRM_DELETE_POSITION': 'Voulez-vous vraiment supprimer cette position ?',

  // positionlist
  'COPY_URL': 'Copier l\'adresse',
  'CV': 'CV',
  'NAME': 'Nom',
  'DATE': 'Date',
  'CANDIDATES_FILTER': 'Profils',

  // candidate form
  'I_ALREADY_HAVE_ACCOUNT': 'J\'ai déjà un compte',
  'CREATE_ACCOUNT_FOR_EMAIL': 'Vous allez créer un nouveau compte pour l\'email ',
  'EMAIL_SENT': 'Un email vous a été envoyé sur votre compte ',
  'TO_CONNECT': ' pour vous connecter.',

  // position creation form
  'INTERNAL_NOTE': 'Famille',

  // timeline
  'CANDIDATE_APPLIED_TO': 'Le candidat a postulé à l\'étape',
  'SENT_TO': 'a envoyé',
  'TO_STEP': 'à l\'étape',
  'REJECTED_CANDIDATE': 'a rejeté la candidature de',
  'FOR_POSITION': 'au poste de',
  'ARCHIVED_CANDIDATE': 'a archivé la candidature de',
  'CONNECTED_TO_STEP': 'Le candidat s\'est connecté à l\'étape',
  'WROTE': 'a indiqué',
  'SEND_PRIVATE_MAIL': 'a envoyé un mail privé',

  // CandidatePositionDetail
  'BACK': 'Retour',
  'NO_UPDATE_TO_DO': 'Aucune modification à faire',

  // CandidateSkills
  'NO_SKILL': 'Vous n\'avez enregistré aucune compétence',

  // positionList
  'NO_POSITION': 'Vous n\'avez postulé à aucune offre',

  // positionList
  'COMPANY_NO_POSITION': 'Vous n\'avez créé aucune offre',

  // editStep
  'DRAG_SKILL_HERE': 'Glissez-déposer ici les compétences',

  // Minigame 01
  'INTRO_MINIGAME_01': ' Pour ce test, nous allons mesurer votre impulsivité. \n\nCliquez sur le bouton vert seulement lorsque vous voyez un coeur.',
  'START': 'Commencer',
  'FINISH': 'C\'est fini !!!',
  'NO_HEART': 'Vous n\'avez pris aucun coeur',
  'ONE_HEART': 'Vous avez pris un coeur',
  'MANY': 'Vous avez pris',
  'HEARTS': 'coeurs',
  'NO_FISH': 'et aucun poisson',
  'ONE_FISH': 'et un poisson',
  'AND': 'et',
  'FISHED': 'poissons',
  'ONLY_TOUCH_HEART': 'Ne touchez que si c\'est un coeur',

  // questionnaireCreation
  'CHECKBOX': 'Choix multiple',
  'RADIO': 'Choix unique',
  'ORDERING': 'Ordering',
  'TEXTAREA': 'Zone de texte',
  'VIDEO': 'Video',
  'IMAGE': 'Image',

  'CARD': 'Carte',
  'BASIC': 'Basique',

  // questionnairesList
  'SURVEYS_LIST': 'Liste des questionnaires',

  // Edit Questionnaire
  'CONFIRM_DELETE_SURVEY': 'Voulez-vous vraiment supprimer ce questionnaire ?',

  // email window
  'EMAIL_TITLE': 'Contacter le/les candidats par email',
  'SUBJECT': 'Sujet',
  'MESSAGE': 'Message'
};
