import React from 'react'
import { withKnobs, color, text } from '@storybook/addon-knobs'

import BackgroundLayout from '..'

export default {
  title: 'BackgroundLayout',
  decorators: [withKnobs],
  parameters: {
    component: BackgroundLayout,
  },
}

export const main = () => (
  <BackgroundLayout
    backgroundImg={text(
      'backgroundImg',
      'https://s3-eu-west-1.amazonaws.com/rhtool/image/BG_detective.png'
    )}
    backgroundColor={color('backgroundColor', '#908787')}
  >
    Content
  </BackgroundLayout>
)
