export default {
  'OFFER_LIST': 'Position List',
  'NOT_YET_PUBLISHED': 'Not yet published',

  'JOB_TITLE': 'Job title',
  'JOB_DESCRIPTION': 'Job description',
  'JOB_OFFER': 'Position',
  'LIST_OF_CANDIDATES': 'Staff members',
  'SETTINGS': 'Settings',

  'EMAIL': 'Email',
  'PASSWORD': 'Password',
  'CONTINUE': 'Continue',
  'CREATE_ACCOUNT': 'Create an account',
  'LINKEDIN_CONNECTION': 'Connect with Linkedin',
  'FACEBOOK_CONNECTION': 'Connect with Facebook',
  'GOOGLE_CONNECTION': 'Connect with Google',
  'GITHUB_CONNECTION': 'Connect with Github',

  // candidate Indentity && position details && survey
  'PROFIL_PICTURE': 'Profile picture',
  'SELECT_PICTURE': 'Select a picture',
  'JOB_DETAILS': 'Position details',
  'COMPANY': 'Company',
  'IDENTITY': 'Identity',
  'CANDIDATES': 'Candidates',
  'CANDIDATE_NAME': 'Last Name',
  'CANDIDATE_FIRSTNAME': 'First Name',
  'CANDIDATE_MAIL': 'Email',
  'CANDIDATE_COUNTRY': 'Country',
  'CANDIDATE_ADDRESS': 'Address',
  'CANDIDATE_TELEPHONE': 'Telephone',
  'CANDIDATE_CITY': 'Town',
  'CANDIDATE_ZIP': 'ZIP Code',
  'SCHOOL_AND_EXPERIENCES': 'School and experiences',
  'MAIN_SCHOOL': 'Main School',
  'YEAR_EXPERIENCE': 'Years of experience in the job',
  'TOTAL_YEAR_EXPERIENCE': 'Total years of experience',
  'SCHOOL': 'School',
  'NO_CANDIDATE_YET': 'None have replied...',
  'COPIED': 'Copied!',

  'VALIDATE': 'Validate',
  'PUBLISHED_ON': 'published on ',
  'NOT_PUBLISHED': 'not yet published',
  'SELECT_ALL': 'Select all',
  'NEXT_STEP': 'Go to next step',
  'SEND_MESSAGE': 'Send Message',
  'ARCHIVE': 'Archive',
  'REFUSE': 'Refuse',
  'ACTIONS': 'Actions',
  'START_DATE': 'Start date',
  'END_DATE': 'End Date',
  'POSITION_TYPE': 'Position type',
  'SKILLS': 'Skills',
  'DELETE': 'Delete',
  'EDIT': 'Modify',
  'EDIT_STEPS': 'Modify Steps',

  // Special skills
  'IMPORT_CV': 'Please import your CV',
  'IMPORT_PORTFOLIO': 'Please import your Portfolio',
  'FILE_SELECTED': 'File selected :',
  'INSTRUCTION_DRAG_CV': 'Drag and drop your file here in word or pdf format',
  'INSTRUCTION_REPLACE_CV': 'Drag and drop your file to replace the current file',
  'INSTRUCTION_DRAG_PORTFOLIO': 'Drag and drop your file here in pdf, jpg or png format',
  'INSTRUCTION_REPLACE_PORTFOLIO': 'Drag and drop your file to replace the current file',
  'AVAILABILITY_DATE': 'What is your availability start?',
  'ADD_FILE': 'Add a file',
  'LENGTH': 'Length',
  'AVAILABILITY': 'Availability',

  // APPLY CANDIDATE
  'LEGAL_NOTES': 'Legal notes',
  'ERROR_FILE_CV': 'The file must be in word or pdf format',
  'ERROR_FILE_PORTFOLIO': 'The file must be in pdf, jpg or png format',
  'VALIDATE_APPLICATION': 'Do you want to validate the form ?',
  'YES': 'Yes',
  'NO': 'No',
  'FIRSTNAME': 'First Name',
  'LASTNAME': 'Last Name',
  'PHONE': 'Telephone',
  'EMAIL_NOT_VALID': 'Your email is not a valid email',
  'INCOMPLETE_FIELDS': 'You did not fill the following fields : ',
  'OK': 'Ok',
  'APPLICATION_SENT_TITLE': 'Your application has been sent !',

  // Candidate position detail
  'APPLICATION_SENT_TEXT': 'We are going to process it and we will send you an answer soon !',
  'INFO_UPDATED': 'Informations have been updated',
  'CLOSE': 'Close',

  // Candidate detail
  'SEND_TO': 'Send ',
  'TO_NEXT_STEP': 'to next step ?',
  'SEND_EMAIL_REFUSE': 'Send an refuse email to ',
  'CANCEL': 'Cancel',

  // SKill Dialog
  'NAME_SKILL': 'Name of the skill',
  'QUESTION_SKILL': 'Question on the skill',
  'ADD_SKILLS': 'Add skills',

  // Position metrics
  'RESPONSE_RATE': 'Response rate',

  // Position detail component
  'TARGET_CANDIDATE': 'Ideal profile',
  'NUMBER_CANDIDATES': 'nbre candidates',
  'FULL_APPLY_METRIC': 'Full application',
  'LOGIN_METRIC': 'Login',
  'APPLY_METRIC': 'Arrived on site',

  // Edit step skill special
  'SPECIAL_FIELDS': 'Specific questions',

  // positionInfo
  'IN_PROGRESS': 'in progress',
  'ARCHIVED': 'archived',
  'DELETED': 'deleted',
  'OVER': 'over',
  'ANONYMOUS': 'Anonymous',

  // actionButton
  'SUGGEST_JOB': 'suggest a job',

  // candidateList
  'SEE_PORTFOLIO': 'Portfolio',

  // vivier
  'RESULTS': 'results',
  'SORT_BY_DATE': 'Sort by date',
  'KEYWORDS': 'Key words',

  // CandidateIdentityinfo
  'REQUIRED': 'Required',
  'EMAIL_EXISTS': 'This email is already used, please use another one or connect to the other account',

  // customEvent
  'ADD_ACTION_TIMELINE': 'Add an action to the timeline',
  'COLOR': 'Color',
  'YOUR_ACTION': 'Your action',

  // SkillSpecialDialog
  'ADD_SPECIAL_QUESTION': 'Add a special question',

  'STEP': 'Step ',

  // clock
  'HOURS': 'Hours',
  'MINUTES': 'Minutes',
  'SECONDS': 'Seconds',

  // Portfolio
  'PORTFOLIO': 'Portfolio',

  // login.hmtl
  'ERROR_EMAIL': 'Please insert an email',
  'ERROR_PASSWORD': 'Please insert a password',

  // position creation form
  'COUNTRY': 'Country',
  'ADDRESS': 'Address',
  'TELEPHONE': 'Telephone',
  'CITY': 'Town',
  'ZIP': 'ZIP code',

  'COMPULSORY_FIELD': 'This field is compulsory',

  // panelHeader
  'IMPORTANT': 'Important',
  'SECONDARY': 'Wish',
  'OPTION': 'Option',
  'TOP': 'Top',
  'BOTTOM': 'Bottom',
  'BEFORE': 'Before',
  'AFTER': 'After',
  'BEFORE_LONG_TITLE': 'Special requirements',
  'AFTER_LONG_TITLE': 'Additional requirements',
  'OPTION_LONG_TITLE': 'Optional',

  // Nav
  'CANDIDATE_SPACE': 'I am a Candidate',
  'RECRUITER_SPACE': 'Recruiter\'s login',
  'POSITIONS': 'Positions',
  'CANDIDATE_PROFILE': 'My info',
  'SKILLS_MENU': 'Skills',
  'CANDIDATES_LIST': 'Candidates',
  'CANDIDATE_POOL': 'Staff members',
  'SURVEYS': 'Skills survey',

  // ButtonParam
  'DISCONNECT': 'Disconnect',
  'PARAMETERS': 'Parameters',
  'FRENCH': 'French',
  'ENGLISH': 'English',
  'CHINESE': 'Chinese',
  'ARABIC': 'Arabe',

  // CandidateForm index
  'SIGN_IN': 'Sign In',
  'SIGN_UP': 'Sign Up',
  'SIGN_WITH': 'Connect with',
  'I_FORGOT_MY_PASSWORD': 'I forgot my password',
  'SIGN_IN_OR_UP': 'Type your email to sign in or create an account',

  // applyPosition
  'PREVIOUS': 'Previous',
  'NEXT': 'Next',
  'DONE': 'Done',
  'I_ACCEPT_THE': 'I accept the',
  'POSITION_NOT_FOUND': 'Oops the position does not exist',

  // login company
  'ERROR_MATCHING_EMAIL_PASSWORD': 'The email and password don\'t match',

  'ACCOUNT_ALREADY_EXISTS': 'The account already exists',

  'YOU_WILL_APPLY_TO_STEP': 'You\'re going to apply to step ',
  'IN_A_FEW_CLICKS': ' in a few clicks (1mn)',

  // ScoringPanel
  'SEE_CV': 'See CV',
  'COMPULSORY': 'Compulsory',

  // parameters
  'PASSWORDS_DONT_MATCH': 'Passords don\'t match',
  'CONFIRM_PASSWORD': 'Confirm the password',
  'CHANGE_PASSWORD': 'Change the password',
  'CONFIRM_DELETE_ACCOUNT': 'Do you really want to delete your account ?',
  'CANDIDATES_ANONYMISED': 'Candidates anonymised',
  'DELAY_MONTHS': 'Delay (months)',

  // Edit Step
  'CONFIRM_DELETE_STEP': 'Remove this step ?',

  // Edit Position
  'CONFIRM_DELETE_POSITION': 'Remove this position ?',

  // positionlist
  'COPY_URL': 'Copy URL',
  'CV': 'CV',
  'NAME': 'Name',
  'DATE': 'Date',
  'CANDIDATES_FILTER': 'Staff members',

  // candidate form
  'I_ALREADY_HAVE_ACCOUNT': 'I already have an account',

  'CREATE_ACCOUNT_FOR_EMAIL': 'You\'re going to create an account for the email ',
  'EMAIL_SENT': 'An email was sent to your account ',
  'TO_CONNECT': ' to connect.',

  // position creation form
  'INTERNAL_NOTE': 'Internal Note',

  // timeline
  'CANDIDATE_APPLIED_TO': 'The candidate applied to step',
  'SENT_TO': 'sent to',
  'TO_STEP': 'to the step',
  'REJECTED_CANDIDATE': 'rejected the application of',
  'FOR_POSITION': 'for the position',
  'ARCHIVED_CANDIDATE': 'archived the application of',
  'CONNECTED_TO_STEP': 'The candidate connected to the step',
  'WROTE': 'wrote',
  'SEND_PRIVATE_MAIL': 'send a private message',

  // CandidatePositionDetail
  'BACK': 'Back',
  'NO_UPDATE_TO_DO': 'There is nothing to update',

  // CandidateSkills
  'NO_SKILL': 'You have no skill registered yet',

  // positionList
  'NO_POSITION': 'You didn\'t apply to any position yet',
  // positionList
  'COMPANY_NO_POSITION': 'You didn\'t create any position',

  // editStep
  'DRAG_SKILL_HERE': 'Drag and drop the skill here',

  // Minigame 01
  'INTRO_MINIGAME_01': ' For this test we will measure your impulsivity \n\nClick on the green button only when the image is a heart.',
  'START': 'Start',
  'FINISH': 'Game over',
  'NO_HEART': 'You took no heart',
  'ONE_HEART': 'You took one heart',
  'MANY': 'You took',
  'HEARTS': 'hearts',
  'NO_FISH': 'and no fish',
  'ONE_FISH': 'and one fish',
  'AND': 'and',
  'FISHED': 'fishes',
  'ONLY_TOUCH_HEART': 'Only tap if it\'s a heart',

  // questionnaireCreation
  'CHECKBOX': 'Multiple choice',
  'RADIO': 'Single choice',
  'ORDERING': 'Ordering',
  'TEXTAREA': 'Text box',
  'VIDEO': 'Video',
  'IMAGE': 'Image',

  'CARD': 'Card',
  'BASIC': 'Basic',

  // questionnairesList
  'SURVEYS_LIST': 'Surveys List',

  // Edit Questionnaire
  'CONFIRM_DELETE_SURVEY': 'Remove this position ?',

  // email window
  'EMAIL_TITLE': 'Send email',
  'SUBJECT': 'Subject',
  'MESSAGE': 'Message'
};
