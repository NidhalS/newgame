import React from 'react'
import { action } from '@storybook/addon-actions'
import { withKnobs, number, boolean } from '@storybook/addon-knobs'
import data from '../../Memory/stories/data.json'

import MemoryBlock from '..'

export default {
  title: 'MemoryBlock',
  decorators: [withKnobs],
  parameters: {
    component: MemoryBlock,
  },
}

const scoreOptions = {
  range: true,
  min: 0,
  max: 1,
  step: 0.1,
}

export const main = () => (
  <MemoryBlock
    block={data.blocks[0]}
    rounds={[1, 2, 3, 2, 3]}
    currentRound={number('currentRound', 0, {
      min: 0,
      max: 4,
    })}
    goodScore={number('goodScore', 0.5, scoreOptions)}
    badScore={number('badScore', 0.5, scoreOptions)}
    lang="fr"
    onAnswer={action('onAnswer')}
    onFinish={action('onFinish')}
  />
)
