import api from '../../app/shared/constants/api'
import axios from 'axios'

/**
 * [WIP]
 * Check if a candidate can access to the session and get the authentification token
 * POST
 * @param {*} data
 */
const loginSession = async ({ positionId, stepId, candidateId }) => {
  const res = await axios.put(`${api.url}/me/loginSession`, {
    positionId,
    stepId,
    candidateId,
  })

  // TODO Auth. We still use the angularJS part to handle authentication.

  return res
}

export default { loginSession }
