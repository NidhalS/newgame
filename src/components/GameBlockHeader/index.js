import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GameMeter from '../GameMeter'
import GameTimer from '../GameTimer'

const Wrapper = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: 100%;
`

/**
 * Wrapper around the score & the timer, used in `GameBlock`
 */
const GameBlockHeader = ({ goodScore, badScore, timeout, isTimerRunning }) => {
  return (
    <Wrapper>
      <GameMeter type="good" value={goodScore} />
      <GameTimer timeout={timeout} isRunning={isTimerRunning} />
      <GameMeter type="bad" value={badScore} />
    </Wrapper>
  )
}

GameBlockHeader.propTypes = {
  /**
   * Number from 0 to 1
   */
  goodScore: PropTypes.number.isRequired,
  /**
   * Number from 0 to 1
   */
  badScore: PropTypes.number.isRequired,
  /**
   * How long does it take to answer a question
   */
  timeout: PropTypes.number.isRequired,
  isTimerRunning: PropTypes.bool.isRequired,
}

export default GameBlockHeader
