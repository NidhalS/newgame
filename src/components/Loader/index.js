import React from 'react'
import styled from 'styled-components'
import { CircularProgress } from '@material-ui/core'

const Wrapper = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-around;
  height: 100%;
`

/**
 * Wrapper around material-ui \`CircularProgress\`<br />
 * See https://material-ui.com/api/circular-progress/ for more details
 */
const Loader = () => (
  <Wrapper>
    <CircularProgress size={100} thickness={5} />
  </Wrapper>
)

export default Loader
