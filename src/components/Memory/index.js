import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider, StylesProvider } from '@material-ui/styles'
//import { IntlProvider } from 'react-intl'
import { GlobalStyle, themes } from '../../style'
import BackgroundLayout from '../BackgroundLayout'
import Game from '../Game'
import MemoryBlock from '../MemoryBlock'

import logo from './stories/logo.svg'

// Get all language files


/**
 * Memory mini-game<br />
 * All the props are send by the AngularJS app
 */
const Memory = ({ d, position, lang, onSendDataAndGetResult, onEndGame }) => {
  return (
    // <IntlProvider locale={lang} messages={languageFiles[lang]}>
    <StylesProvider injectFirst>
      <GlobalStyle />
      <ThemeProvider theme={themes[position.company.theme]}>
        <BackgroundLayout
          backgroundImg={position.company.gamePicture}
          backgroundColor={position.company.colorBack}
        >
          <Game
            data={d}
            logo={logo}
            lang={lang}
            onFinishGame={onSendDataAndGetResult}
            onNextGame={onEndGame}
            Block={MemoryBlock}
          />
        </BackgroundLayout>
      </ThemeProvider>
    </StylesProvider>
    // </IntlProvider>
  )
}

Memory.propTypes = {
  /**
   * Data for the mini-game
   */
  d: PropTypes.object.isRequired,
  /**
   * Metadata for the mini-game
   */
  position: PropTypes.shape({
    company: PropTypes.shape({
      gamePicture: PropTypes.string.isRequired,
      colorBack: PropTypes.string.isRequired,
      theme: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  lang: PropTypes.string.isRequired,
  onSendDataAndGetResult: PropTypes.func.isRequired,
  onEndGame: PropTypes.func.isRequired,
}

export default Memory
