import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Fab as MuiFab } from '@material-ui/core'

const Action = styled(MuiFab)`
  width: 100px;
  height: 100px;
  margin: 32px 0 24px;
`

const IconWrapper = styled.span`
  .MuiSvgIcon-root {
    font-size: 50px;
    color: white;
  }
`

/**
 * Overload of Material-UI Fab component to handle icons<br />
 * See https://material-ui.com/api/fab/ for more details
 */
const IconFab = ({ Icon, ...props }) => (
  <Action color="secondary" size="large" {...props}>
    <IconWrapper>{Icon}</IconWrapper>
  </Action>
)

IconFab.propTypes = {
  /**
   * The Icon to show, see https://material-ui.com/components/material-icons/
   */
  Icon: PropTypes.node.isRequired,
}

export default IconFab
