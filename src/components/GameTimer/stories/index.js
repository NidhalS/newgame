import React from 'react'
import styled from 'styled-components'
import { withKnobs, number, boolean } from '@storybook/addon-knobs'
import useRerender from '../../../stories/useRerender'

import GameTimer from '..'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 80px;
`

export default {
  title: 'GameTimer',
  decorators: [withKnobs],
  parameters: {
    component: GameTimer,
  },
}

export const main = () => {
  // False positive error, used to allow the animation to repeat on click
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const props = useRerender()

  return (
    <Wrapper {...props}>
      <GameTimer
        timeout={number('timeout', 9000)}
        isRunning={boolean('isRunning', true)}
      />
    </Wrapper>
  )
}
export const notRunning = () => {
  // False positive error, used to allow the animation to repeat on click
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const props = useRerender()

  return (
    <Wrapper {...props}>
      <GameTimer timeout={9000} isRunning={false} />
    </Wrapper>
  )
}
