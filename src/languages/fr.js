export default {
  'OFFER_LIST': 'Liste des offres',
  'NOT_YET_PUBLISHED': 'Pas encore publiée',

  'JOB_TITLE': 'Intitulé du poste',
  'JOB_DESCRIPTION': 'Description poste',
  'JOB_OFFER': 'Offre d\'emploi',
  'LIST_OF_CANDIDATES': 'Liste candidats',
  'SETTINGS': 'Paramètres',
  

  'EMAIL': 'Saisissez votre adresse e-mail',
  'PASSWORD': 'Mot de passe',
  'CONTINUE': 'Continuer',
  'CREATE_ACCOUNT': 'Créer un compte',
  'LINKEDIN_CONNECTION': 'Se connecter avec Linkedin',
  'FACEBOOK_CONNECTION': 'Se connecter avec Facebook',
  'GOOGLE_CONNECTION': 'Se connecter avec Google',
  'GITHUB_CONNECTION': 'Se connecter avec Github',

  // candidate Indentity && position details && survey
  'PROFIL_PICTURE': 'Photo du profil',
  'SELECT_PICTURE': 'Sélectionnez une photo',
  'JOB_DETAILS': 'details de l\'offre',
  'COMPANY': 'Société',
  'IDENTITY': 'Identité',
  'CANDIDATES': 'candidats',
  'CANDIDATE_NAME': 'Nom',
  'CANDIDATE_FIRSTNAME': 'Prénom',
  'CANDIDATE_SPECIAL_ID': 'Identifiant',
  'CANDIDATE_MAIL': 'Email',
  'CANDIDATE_COUNTRY': 'Pays',
  'CANDIDATE_ADDRESS': 'Adresse',
  'CANDIDATE_TELEPHONE': 'Téléphone',
  'CANDIDATE_CITY': 'Ville',
  'CANDIDATE_ZIP': 'Code postal',
  'SCHOOL_AND_EXPERIENCES': 'Formation et experiences',
  'MAIN_SCHOOL': 'Formation principale',
  'YEAR_EXPERIENCE': 'Années d\'expérience dans le métier',
  'TOTAL_YEAR_EXPERIENCE': 'Années d\'expérience totales',
  'SCHOOL': 'Formation',
  'NO_CANDIDATE_YET': 'Aucun candidat n\'a encore répondu...',
  'COPIED': 'Copié!',
  'NOSTATUS': 'Sans statut',

  'VALIDATE': 'Valider',
  'PUBLISHED_ON': 'publiée le ',
  'NOT_PUBLISHED': 'pas encore publiée',
  'SELECT_ALL': 'Tout sélectionner',
  'FILTER_ARCHIVED': 'Avec les candidats archivés',
  'FILTER_NOT_APPLIED': 'Avec les candidats sans score',
  'NEXT_STEP': 'Envoyer à l\'étape suivante',
  'SEND_MESSAGE': 'Envoyer un message',
  'ARCHIVE': 'Archiver',
  'REFUSE': 'Refuser',
  'ACTIONS': 'Actions',
  'START_DATE': 'Date début',
  'END_DATE': 'Date fin',
  'POSITION_TYPE': 'Type de poste',
  'SKILLS': 'Compétences',
  'DELETE': 'Supprimer',
  'EDIT': 'Modifier',
  'EDIT_STEPS': 'Modifier Etapes',
  'SEND': 'Envoyer',
  'TEMPLATE': 'Modèle',
  'CREATE_TEMPLATE': 'Ajouter aux modèles',
  'NEW_TEMPLATE': 'Nouveau modèle',
  'TEMPLATE_NAME': 'Nom du modèle',
  'TEMPLATE_PARAM': 'Paramètres du modèle',
  'DELETE_TEMPLATE_TITLE': 'Supprimer le modèle',
  'DELETE_TEMPLATE': 'Etes-vous sur de vouloir supprimer ce modèle ?',
  'VALIDATE_TEMPLATE': 'Le modèle a été mis à jour.',
  'VALIDATE_TEMPLATE_ERROR': 'Le modèle n\'a pas pu être mis à jour.',
  'VALIDATE_CREATION_TEMPLATE': 'Le modèle a été créé.',
  'VALIDATE_CREATION_TEMPLATE_ERROR': 'Le modèle n\'a pas pu être créé.',
  'VALIDATE_DELETE_TEMPLATE': 'Le modèle a bien été supprimé.',
  'VALIDATE_DELETE_TEMPLATE_ERROR': 'Le modèle n\'a pas pu être supprimé.',

  // Special skills
  'IMPORT_FILE': 'Veuillez importer votre fichier',
  'IMPORT_CV': 'Veuillez importer votre CV',
  'IMPORT_PORTFOLIO': 'Veuillez importer votre Portfolio',
  'FILE_SELECTED': 'Ficher sélectionné :',
  'INSTRUCTION_DRAG_CV': 'Glisser et déposer un fichier au format word doc, docx ou pdf',
  'INSTRUCTION_REPLACE_CV': 'Drag and droppez pour remplacer le CV enregistré',
  'INSTRUCTION_REPLACE_FILE': 'Drag and droppez pour remplacer le fichier',
  'INSTRUCTION_DRAG_PORTFOLIO': 'Glisser et déposer un fichier au format pdf, jpg ou png',
  'INSTRUCTION_DRAG_IMG': 'Glisser et déposer un fichier au format jpg ou png',
  'INSTRUCTION_DRAG_FILE': 'Glisser et déposer un fichier au format pdf, doc, docx, jpg ou png',
  'INSTRUCTION_REPLACE_PORTFOLIO': 'Glisser et déposer pour remplacer le Portfolio enregistré',
  'AVAILABILITY_DATE': 'Quelles sont vos dates de disponibilités ?',
  'ADD_FILE': 'Ajouter un fichier',
  'LENGTH': 'Durée du stage (en mois)',
  'AVAILABILITY': 'Disponibilité',
  'BIRTH_DATE': 'Date de naissance',

  // APPLY CANDIDATE
  'LEGAL_NOTES': 'mentions légales',
  'ERROR_FILE_CV': 'Le fichier doit être au format doc, docx ou pdf',
  'ERROR_FILE_PORTFOLIO': 'Le fichier doit être au format pdf, jpg ou png',
  'ERROR_FILE_CUSTOM': 'Le fichier doit être au format pdf, jpg, png, doc ou docx',
  'VALIDATE_APPLICATION': 'Valider le questionnaire ?',
  'YES': 'Oui',
  'NO': 'Non',
  'FIRSTNAME': 'Prénom',
  'LASTNAME': 'Nom',
  'PHONE': 'Téléphone',
  'EMAIL_NOT_VALID': 'Votre email n\'est pas un email valide',
  'INCOMPLETE_FIELDS': 'Vous n\'avez pas rempli le(s) champ(s) suivant(s): ',
  'OK': 'Ok',
  'APPLICATION_SENT_TITLE': 'Votre candidature a été envoyée !',
  'VALIDATE_STAR' : 'Désolé, les conditions grisées sont obligatoires pour pouvoir continuer',
  'QUIT_APPLICATION' : 'Abandonner',

  // Candidate position detail
  'APPLICATION_SENT_TEXT': 'Nous allons la traiter et nous vous enverrons une réponse dans les plus brefs délais !',
  'INFO_UPDATED': 'Informations mises à jour',
  'CLOSE': 'Fermer',
  'CLOSE_FEED':'Terminer',

  // Candidate detail
  'ACCOUNT_CHECKED' : 'compte validé',
  'SAVE':'Enregistrer',
  'SEND_TO': 'Faire passer ',
  'TO_NEXT_STEP': 'à l\'étape suivante ?',
  'SEND_EMAIL_REFUSE': 'Envoyer un mail de refus à',
  'CANCEL': 'Annuler',
  'DOWNLOAD_SCORE_PDF': 'Télécharger les résultats',
  'MAKE_PASS_AGAIN': 'Faire repasser',
  'ADD_STATUS':'Ajouter statut',
  'EDIT_STATUS':'Modifier un statut',
  'ERROR_CHANGE_STATUS': 'Vous ne pouvez pas modifier ce statut',
  'ERROR':'Erreur',
  // SKill Dialog
  'NAME_SKILL': 'Nom de la compétence',
  'QUESTION_SKILL': 'Question sur la compétence',
  'ADD_SKILLS': 'Ajout compétences',
  'TITLE_CATEGORY': 'Catégorie',
  'SEARCH_SKILL' : 'Chercher la compétence',

  // Position metrics
  'RESPONSE_RATE': 'Taux de réponse',

  // Position detail component
  'TARGET_CANDIDATE': 'Candidat idéal',
  'NUMBER_CANDIDATES': 'nbre candidats',
  'FULL_APPLY_METRIC': 'Candidature(s) complète(s)',
  'LOGIN_METRIC': 'Login',
  'APPLY_METRIC': 'Arrivé(s) sur le site',

  // Edit step skill special
  'SPECIAL_FIELDS': 'questions spécifiques',

  // positionInfo
  'IN_PROGRESS': 'en cours',
  'ARCHIVED': 'archivé(s)',
  'DELETED': 'supprimé(s)',
  'OVER': 'sur',
  'ANONYMOUS': 'Anonyme',
  'SOONER_FIRST': 'Plus récents',
  'LATER_FIRST': 'Moins récents',
  'NO_SORT': 'Pas de tri',
  'MAP': 'Carte',

  // actionButton
  'SUGGEST_JOB': 'proposer un poste',

  // candidateList
  'DOWNLOAD_PORTFOLIO': 'Télécharger le Portfolio',
  'NO_PORTFOLIO': 'Pas de portfolio',
  'DOWNLOAD_FILE': 'Télécharger le Fichier',
  'NO_FILE': 'Pas de fichier',

  // vivier
  'RESULTS': 'résultat(s)',
  'SORT_BY_DATE': 'Trier par date',
  'SEARCH_FILTER': 'Email, prénom, nom',
  'KEYWORDS': 'Mots clefs',
  'NO_SORT': 'Tri par score',
  'LATER_FIRST' : 'plus anciens',
  'SOONER_FIRST' : 'plus récents',

  // CandidateIdentityinfo
  'REQUIRED': 'Obligatoire',
  'EMAIL_EXISTS': 'Cet email existe déjà, merci d\'en utiliser un autre ou de vous connecter sur ce compte',

  // customEvent
  'ADD_ACTION_TIMELINE': 'Ajouter une action à la timeline',
  'COLOR': 'Couleur',
  'YOUR_ACTION': 'Votre action',
  'TRAINING': 'S\'entraîner',
  'NEW_ROUND' : 'Nouvelle manche',

  // SkillSpecialDialog
  'ADD_SPECIAL_QUESTION': 'Ajouter une question spéciale',

  'STEP': 'Etape ',

  // clock
  'HOURS': 'Heures',
  'MINUTES': 'Minutes',
  'SECONDS': 'Secondes',

  // Portfolio
  'PORTFOLIO': 'Portfolio',

  // login.hmtl
  'ERROR_EMAIL': 'Merci de rentrer un email valide',
  'ERROR_PASSWORD': 'Merci de rentrer un mot de passe',
  'ACCOUNT_DOESNT_EXIST': 'Ce compte n\'existe pas',
  'ALREADY_HAVE_ACCOUNT_YOU_WILL_RECEIVE_MAIL_WITH_PWD': 'Vous avez déjà un compte avec cet email, voulez-vous recevoir un nouveau mot de passe par mail ?',
  'CANNOT_CONNECT': 'Impossible de se connecter',
  'ACCEPT_EVALUATION' : 'Evaluer votre candidature pour ce poste ?',
  'ACCEPT_CONTACT' : 'Vous proposer d\'autres opportunités ?',
  'COOKIES_TEXT' : 'Nous utilisons des cookies pour sauvegarder votre avancement et faciliter votre navigation.',
  'ACCEPT_USE' : 'Autorisez-vous que vos données servent au recruteur pour :',
  'ACCEPT_LEGAL_NOTICE': 'Vous devez accepter les mentions légales',
  'POPUP_BROWSER' : 'Votre navigateur ne vous permet pas d\'avoir une expérience optimale pour ce parcours en ligne. Nous vous conseillons d\'utiliser Google chrome, Safari ou Firefox.',
  'POPUP_DEVICE' : 'Votre appareil ne vous permet pas d\'avoir une expérience optimale pour ce parcours en ligne. Nous vous conseillons d\'utiliser un smartphone d\'une taille minimale de 4,7 pouces (correspondant à un Iphone 7) ou un ordinateur.',
  'WARNING' : 'Attention',
  // position creation form
  'COUNTRY': 'Pays',
  'ADDRESS': 'Adresse',
  'TELEPHONE': 'Téléphone',
  'CITY': 'Ville',
  'ZIP': 'Code postal',
  'COMPULSORY_FIELD': 'Ce champ est obligatoire',

  // panelHeader
  'IMPORTANT': 'Important',
  'SECONDARY': 'Souhaitable',
  'OPTION': 'Options',
  'TOP': 'Haut',
  'BOTTOM': 'Bas',
  'BEFORE': 'Avant',
  'AFTER': 'Après',
  'BEFORE_LONG_TITLE': 'Questions préliminaires',
  'AFTER_LONG_TITLE': 'Questions finales',
  'OPTION_LONG_TITLE': 'Optionnel',

  // Nav
  'CANDIDATE_SPACE': 'Espace candidats',
  'RECRUITER_SPACE': 'Espace recruteurs',
  'POSITIONS': 'Offres',
  'CANDIDATE_PROFILE': 'Mes informations',
  'SKILLS_MENU': 'Compétences',
  'CANDIDATES_LIST': 'Candidats',
  'CANDIDATE_POOL': 'Vivier',
  'SURVEYS': 'Questionnaires',
  'ADMIN': 'Admin',

  // ButtonParam
  'DISCONNECT': 'Se déconnecter',
  'PARAMETERS': 'Paramètres',
  'ACCOUNT_INFO': 'Info Compte',
  'FRENCH': 'Français',
  'ENGLISH': 'Anglais',
  'CHINESE': 'Chinois',
  'ARABIC': 'Arabe',

  // sidenavbar
  'MENU': 'Menu',

  // CandidateForm index
  'SIGN_IN': 'Je me connecte',
  'SIGN_UP': 'Je crée mon compte',
  'SIGN_WITH': 'Vous pouvez également vous connecter avec:',
  'I_FORGOT_MY_PASSWORD': 'J\'ai oublié mon mot de passe',
  'SIGN_IN_OR_UP': 'Entrez votre email',

  // applyPosition
  'PREVIOUS': 'Précédent',
  'NEXT': 'Suivant',
  'DONE': 'Fin',
  'I_ACCEPT_THE': 'J\'accepte les',
  'TIME_OUT': 'Il y a un problème de connexion, ré-essayez plus tard',
  'POSITION_NOT_FOUND': 'Oops la position n\'existe pas',
  'APPLY': 'SE CONNECTER',
  'BEGIN': 'DEMARRER',

  // login company
  'ERROR_MATCHING_EMAIL_PASSWORD': 'l\'email et le mot de passe ne correspondent pas',
  'ERROR_MAX_ATTEMPTS_REACHED': 'Trop de requêtes ont été faites, veuillez attendre', 

  'ACCOUNT_ALREADY_EXISTS': 'Le compte existe déjà',

  'YOU_WILL_APPLY_TO_STEP': 'Vous allez postuler à l\'étape ',
  'IN_A_FEW_CLICKS': ' en quelques clics (1mn)',

  // ScoringPanel
  'DOWNLOAD_CV': 'Télécharger le CV',
  'NO_CV': 'Pas de CV',
  'COMPULSORY': 'Obligatoire',
  'NO_SKILLS': ' Pas de compétences pour le candidat idéal',
  'BY_DEFAULT': 'Par défaut',

  // parameters
  'PASSWORDS_DONT_MATCH': 'Les mots de passe ne correspondent pas',
  'CONFIRM_PASSWORD': 'Confirmer le mot de passe',
  'CHANGE_PASSWORD': 'Changer le mot de passe',
  'CONFIRM_DELETE_ACCOUNT': 'Voulez-vous vraiment supprimer votre candidature ?',
  'WE_SEND_YOU_AN_EMAIL_CONFIRMATION': 'Un email vous a été envoyé.',
  'TITLE':'Intitulé',
  'ASSOCIED_COLOR':'Couleur associeée',
  'ADD_TO_COLLECT':'Ajouter à la collecte ',
  'ICON':'Icone',
  'DELAY_MONTHS': 'Délais (mois)',
  'ANONYMIZATION' : 'Anonymisation',
  'ANONYMISE': 'Anonymiser',
  'CANDIDATES_ANONYMISED': 'Candidats anonymisés',

  // Edit Step
  'CONFIRM_DELETE_STEP': 'Voulez-vous vraiment supprimer cette étape ?',

  // Edit Position
  'CONFIRM_DELETE_POSITION': 'Voulez-vous vraiment supprimer cette position ?',

  // positionlist
  'COPY_URL': 'Copier l\'adresse',
  'CV': 'CV',
  'NAME': 'Nom',
  'DATE': 'Date',
  'CANDIDATES_FILTER': 'Candidats',

  // candidate form
  'I_ALREADY_HAVE_ACCOUNT': 'J\'ai déjà un compte',
  'CREATE_ACCOUNT_FOR_EMAIL': 'Vous allez créer un nouveau compte pour l\'email ',
  'EMAIL_SENT': 'Un email vous a été envoyé sur votre compte ',
  'TO_CONNECT': ' pour vous connecter.',

  // position creation form
  'INTERNAL_NOTE': 'Note interne',

  // timeline
  'CANDIDATE_ACCOUNT_CREATED': 'Le compte candidat est créeé',
  'CANDIDATE_APPLIED_TO': 'Le candidat a postulé à l\'étape',
  'CANDIDATE_START_TIMER': 'Le candidat a commencé l\'étude de cas',
  'SENT_TO': 'a envoyé',
  'WAS_SENT': 'a été envoyé',
  'TO_STEP': 'à l\'étape',
  'REJECTED_CANDIDATE': 'a rejeté la candidature de',
  'FOR_POSITION': 'au poste de',
  'ARCHIVED_CANDIDATE': 'a archivé la candidature de',
  'CONNECTED_TO_STEP': 'Le candidat s\'est connecté à l\'étape',
  'CANDIDATE_DELETED': 'Le candidat a supprimé sa candidature',
  'WROTE': 'a indiqué',
  'SEND_PRIVATE_MAIL': 'a envoyé un mail privé',
  'SEND_FEEDBACK_BY_STAFF': 'a envoyé un feedback textuel',
  'SEND_REDO_STEP_BY_STAFF': 'a envoyé un lien pour repasser les tests',
  'DEFINE_STATUS':'Définir un statut',
  'COMMENT':'Commentaire',


  // CandidatePositionDetail
  'BACK': 'Retour',
  'NO_UPDATE_TO_DO': 'Aucune modification à faire',
  'SCORE': 'Score',
  'PROFILE': 'Profil',
  'GLOBAL_SCORE': 'Score global',
  'DETAIL_GLOBAL_SCORE': 'Détails score global',
  'PROGRESS': 'Avancement',
  'REST': 'Reste',
  'SEE_DETAIL': 'Voir détails',
  'IN_TIME' : 'Temps total',
  'OUT_OF_SCORE' : 'Hors score',
  'PDF_SETTINGS': 'Paramètres du pdf',
  'PDF_ZONE_1': 'Afficher compétences et questions spéciales',
  'PDF_ZONE_2': 'Afficher scores par axe et axes hors score',
  'PDF_ZONE_3': 'Afficher détail des questionnaires',
  'PDF_ZONE_4': 'Afficher détail des réponses hors score',
  'PASS_AUTOMATIC': 'Passage automatique à l\'étape suivante',
  'CONFIRMATION_MAIL' : 'Email de confirmation au candidat',
  'NOTIFY_NEW_CANDIDATES' : 'Me notifier des nouveaux candidats',
  'NOTIFY_WHEN_MAILING_CANDIDATE' : 'Me notifier quand j\'envoie un mail à un candidat',

  // CandidateSkills
  'NO_SKILL': 'Vous n\'avez enregistré aucune compétence',

  // positionList
  'NO_POSITION': 'Vous n\'avez postulé à aucune offre',

  // positionList
  'COMPANY_NO_POSITION': 'Vous n\'avez créé aucune offre',

  // editStep
  'DRAG_SKILL_HERE': 'Glissez-déposer ici les compétences',
  'PONDERATION' : 'pondération',

  // Minigame 01
  'INTRO_MINIGAME_01': ' Pour ce test, nous allons mesurer votre impulsivité. \n\nCliquez sur le bouton vert seulement lorsque vous voyez un coeur.',
  'START': 'Commencer',
  'FINISH': 'C\'est fini !!!',
  'NO_HEART': 'Vous n\'avez pris aucun coeur',
  'ONE_HEART': 'Vous avez pris un coeur',
  'MANY': 'Vous avez pris',
  'HEARTS': 'coeurs',
  'NO_FISH': 'et aucun poisson',
  'ONE_FISH': 'et un poisson',
  'FISHED': 'poissons',
  'ONLY_TOUCH_HEART': 'Ne touchez que si c\'est un coeur',

  // questionnaireCreation
  'CHECKBOX': 'Choix multiple',
  'RADIO': 'Choix unique',
  'ORDERING': 'Ordering',
  'TEXTAREA': 'Zone de texte',
  'VIDEO': 'Video',
  'IMAGE': 'Image',
  'IMAGEMULTI': 'Image choix multiple',
  'BOARD': 'Panneau texte, images, vidéo',

  'CARD': 'Carte',
  'BASIC': 'Basique',
  'CASE_STUDY' : 'Etude de cas',
  'AVAILABLE_DOCUMENTS' :'Documents disponibles',
  'EDIT_POPUP': 'Editer le popup',
  'UPLOAD': 'Importer',
  'FINISH_QUIT': 'Finir et quitter',

  // questionnairesList
  'SURVEYS_LIST': 'Liste des questionnaires',

  // Edit Questionnaire
  'CONFIRM_DELETE_SURVEY': 'Voulez-vous vraiment supprimer ce questionnaire ?',

  // email window
  'EMAIL_TITLE': 'Contacter le/les candidats par email',
  'SUBJECT': 'Sujet',
  'MESSAGE': 'Message',

  // card editor Nathan
  'INSTRUCTION_DRAG_URL_CUSTOM': 'Glissez déposez votre fichier pour ajouter ou remplacer une image ou vidéo',

  // Questionnaire

  'SEND_QUESTIONNAIRE': 'Continuer',
  'QUESTIONNAIRE_NO_NAME': 'Veuillez donner un nom à votre questionnaire',
  'QUESTIONNAIRE_NO_PARTS': 'Nous ne pouvons pas créer le questionnaire, aucune partie n\'a été créée',
  'QUESTIONNAIRE_EMPTY': 'Nous ne pouvons pas créer le questionnaire, il est vide',
  'NO_QUESTIONNAIRE': 'Vous avez créé aucun questionnaire',
  'ADD_ANSWER': 'Précisez',
  'DELETE_COLUMN': 'Voulez-vous supprimer la colonne ?',

  // Etude de cas
  'SUBMIT': 'Soumettre',
  'CONFIRM_ANSWERS': 'Êtes-vous sûr d\'avoir fini ce test ?',
  'SEND_ANSWERS' : 'Envoyer les réponses et passer à l\'étape suivante',
  'STAY_ON_CASE_STUDY' : 'Non, je continue à répondre',
  'NEW_DOCUMENT' : 'Nouveau Document',
  'TIME_IS_OVER': 'Le temps est écoulé, vos réponses sont enregistrées.',

  // WorkMemory

  'INTRO_WORKMEMORY': 'Nous allons mesurer votre capacité à mémoriser des suites de lettres.\nLe niveau de difficulté évoluera au cours du jeu : en fonction du niveau auquel vous serez, vous devrez indiquer s’il s’agit de la même lettre qu’un, deux, ou trois rangs en arrière.\nDans tous les cas, essayez de répondre vite et sans erreur !',
  'END_GAME': 'Bravo, vous avez fini ce jeu !',

  // countdown
  'GO': 'Partez',

  // app.js
  'CONNECTION_LOST': 'Vous n\'avez plus de connexion',

  // questionnaire service
  'TEXT_BETWEEN': 'Le texte doit être compris entre',
  'AND': 'et',
  'TEXT_LESS_THAN': 'Le texte doit être inférieur à',
  'TEXT_GREATER_THAN': 'Le texte doit être supérieur à',
  'CHARACTERS': 'caractères',
  'TEXT_INPUT': 'Entrez votre texte ici',
  'NUMBER_INPUT': 'Entrez votre chiffre ici',

  // Concurrence
  'INTRO_COMPETITION': 'Dans le jeu suivant, vous devrez sélectionner des activités dans lesquelles vous vous projetez',
  'FREEZE': 'Passer en mode statique',
  'UNFREEZE': 'Revenir en mode défilant',
  'LIMIT_OF': 'Maximum de',
  'KEY_WORDS_LIMIT': 'mots-clés atteint',
  'WORD': 'point',
  'WORDS': 'points',


  // Axes
  'AXES': 'Critères',

  // Task switching 2
  'INTRO_TASKSWITCHING2': 'Dans le prochain jeu, vous devrez cliquer sur le bon bouton en fonction de la position de l\'instruction.',
  'ODD': 'Rouge',
  'EVEN': 'Vert',

  'CONSONANT': 'Gauche',

  'VOWEL': 'Droite',

  // London towers
  'START_TOWER' : 'départ',
  'FINISH_TOWER' : 'arrivée',
  'SKIP_GAME' : 'Voulez-vous vraiment abandonner ?',
  // London towers
  'INTRO_BALLOON': 'Dans ce jeu, plus vous gonflerez le ballon plus vous gagnerez, mais attention à l’explosion !',

  'PLAY': 'Jouer',
  'GIVE_UP' : 'Abandonner',

  'INTRO_PERSPECTIVE': 'Dans le prochain jeu, vous devrez comprendre ce que demande votre interlocuteur',

  // minigame end score
  'MINIGAME_END_TEXT_1': 'Bravo, tu as réussi les 2 jeux ! Tu pourrais être un formidable directeur de projet !',
  'MINIGAME_END_TEXT_2': 'Bien joué, tu as réussi un des 2 jeux (Task Switching) ! Tu as toutes les compétences d’un technicien d’études.',
  'MINIGAME_END_TEXT_3': 'Bien joué, tu as réussi un des 2 jeux (Work Memory) ! Tu as toutes les compétences d’un concepteur technique.',
  'MINIGAME_END_TEXT_4': 'Bien joué, tu es assez bon dans les 2 jeux ! Tu ferais un bon un technicien d’études.',
  'YOUR_SCORE_IS': 'Votre score est',

  //Balloon game
  'YOU_EARNED' : 'Vous avez gagné',
  'TOTAL_GAIN' : 'Gain total',

  //anagram game
  'PASS' : 'Passer ce mot',

  //Redo
  'TEST_TO_REDO': 'Choisir le(s) test(s) à relancer',
  
  //company parameter
  'PASSWORD_CONSTRAINT': 'Votre mot de passe doit contenir au minimum 12 caractères, 1 majuscule, 1 chiffre et 1 caractère spécial (!@%^&*#\\-_).',
  'URL_IMG' : 'url de l\'image pour les',
  'GAME' : 'mini jeux',
  'QUESTIONNAIRE' : 'questionnaires',
  'BACK_QUESTIONNAIRE' : 'le fond des questionnaire',
  'LOGIN' : 'login',
  'LOGO' : 'logo',
  'LOGO_MAIL' : 'emails',
  'MAIL_REFUSE' : 'email en cas de refus',
  'MAIL_SUCCESS' : 'email en cas de succés',
  'UPLOAD_FILE' : 'Uploader le fichier',
  'FONT' : 'nom de la police Google Font',
  'THEME_COLOR' : 'Thème couleur préinstallé, mettre custom si vous spécifiez le primaire, accent et warn',
  'PRIMARY' : 'Couleur principale (écrans de textes)',
  'ACCENT' : 'Couleur des boutons et des contours',
  'WARN' : 'Couleur des erreurs',
  
  'APPLY_TEXT' : 'texte en introduction de la position',
  'APPLY_TEXT_END' : 'texte à la fin du parcours',
  'SUBJECT_EMAIL' : 'sujet de l\'email envoyé au candidat (sera suivi du nom de la position)',
  'HEADER_SUCCESS' : 'header de l\'email envoyé au passage d\'une étape (par défaut)',
  'SUCCESS_MAIL' : 'contenu de l\'email envoyé au passage d\'une étape (par défaut)',
  'BUTTON_SUCCESS' : 'bouton de l\'email envoyé au passage d\'une étape (par défaut) pour faire l\'étape suivante',
  'HEADER_REFUSE' : 'header de l\'email envoyé au passage d\'une étape (par défaut)',
  'REFUSE_MAIL' : 'contenu de l\'email envoyé au passage d\'une étape (par défaut)',
  'BUTTON_REFUSE' : 'bouton de l\'email envoyé au passage d\'une étape (par défaut) pour faire l\'étape suivante',
  'STATUS': 'Statuts',
  'LEGAL_MENTIONS' : 'Mentions Légales',
  'LEGAL_MENTIONS_TEXT' : 'Ce site Internet est édité par la société Goshaba, une société par actions simplifiée au capital de 10.000 euros dont le siège social est situé \n11, rue Clavel, 75019 Paris, immatriculée au registre du commerce et des sociétés de Paris sous le numéro 804 620 359. \nDirectrice de la publication : Camille Morvan. Numéro de TVA intracommunautaire : FR84804620359',
  'OTHER' :'Autre',
  //feed back parameters
  'SEND_FEEDBACK' : 'Envoyer le feedback',
  'SEND_FEEDBACK_IN_CANDIDATE_LANG' : 'Envoyer le feedback',
  'DOWNLOAD_CANDIDATE' : 'Télécharger la liste des candidats',
  'SEND_EMAIL_VALIDATION' : 'envoyer demande de validation d\'email',

  //accountInfo
  'STAFF_MASTER': 'Utiliser les paramètres du compte administrateur',

  //PlayfulSpirit
  'I_AM': 'Je suis très',

  //React Session
  'ADD_CANDIDATE': 'Ajouter un candidat',
  'ROWS_PER_PAGE': 'Lignes par page',
  'CANDIDATE_LIST': 'Liste de candidats',
  'IP_WHITE_LIST': 'Liste des sites',
  'ADD_IP': 'Ajouter une ip',
  'LOG_IN_TEXT': 'Veuillez vous connecter',
  'LOG_IN_BUTTON': 'Se connecter',
  'APPLICANT_ID': 'Identifiant',

  //others
  'COLON': ' :',

};
