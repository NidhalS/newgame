import { createMuiTheme } from '@material-ui/core/styles'
import teal from '@material-ui/core/colors/teal'
import blue from '@material-ui/core/colors/blue'
import red from '@material-ui/core/colors/red'
import orange from '@material-ui/core/colors/orange'
import brown from '@material-ui/core/colors/brown'
import grey from '@material-ui/core/colors/grey'

// All the other shades are available in those packages, see: https://material-ui.com/customization/color
// import pink from '@material-ui/core/colors/pink'
// import purple from '@material-ui/core/colors/purple'
// import deepPurple from '@material-ui/core/colors/deepPurple'
// import indigo from '@material-ui/core/colors/indigo'
// import lightBlue from '@material-ui/core/colors/lightBlue'
// import cyan from '@material-ui/core/colors/cyan'
// import teal from '@material-ui/core/colors/teal'
// import green from '@material-ui/core/colors/green'
// import lightGreen from '@material-ui/core/colors/lightGreen'
// import lime from '@material-ui/core/colors/lime'
// import yellow from '@material-ui/core/colors/yellow'
// import amber from '@material-ui/core/colors/amber'
// import deepOrange from '@material-ui/core/colors/deepOrange'

// TODO: Adjust colors because we can't set all the shade like in AngularJS Material
// e.g:
// const alfredPrimary = {
//   light: '#AFD2E9',
//   main: '#236da8',
//   dark: '#133b5b',
//   contrastText: '#fff',
// }
// You can use the official material design tool to set them: https://material.io/resources/color
const alfredPrimary = { main: '#236da8' }
const alfredAccent = { main: '#81A3BE' }

export const theme = createMuiTheme({
  palette: {
    primary: alfredPrimary,
    secondary: alfredAccent,
    warn: red,
  },
})

export const alfred = createMuiTheme({
  palette: {
    primary: alfredPrimary,
    secondary: alfredAccent,
    warn: red,
  },
})

export const detective = createMuiTheme({
  palette: {
    primary: brown,
    secondary: orange,
    warn: red,
  },
})

const lvmhPrimary = grey
const lvmhAccent = grey
export const lvmh = createMuiTheme({
  palette: {
    primary: lvmhPrimary,
    secondary: lvmhAccent,
    warn: red,
  },
})

const racePrimary = orange
const raceAccent = orange
export const race = createMuiTheme({
  palette: {
    primary: racePrimary,
    secondary: raceAccent,
    warn: raceAccent,
  },
})

const lorealPrimary = grey
const lorealAccent = grey
const lorealWarn = grey
export const loreal = createMuiTheme({
  palette: {
    primary: lorealPrimary,
    secondary: lorealAccent,
    warn: lorealWarn,
  },
})

const airPrimary = blue
const airAccent = blue
const airWarnin = blue
export const air = createMuiTheme({
  palette: {
    primary: airPrimary,
    secondary: airAccent,
    warn: airWarnin,
  },
})

const bcgPrimary = Object.assign(
  { ...teal },
  {
    '300': '#b2dfdb',
    '500': '#29ba74',
  }
)
const bcgSecondary = Object.assign(
  { ...teal },
  {
    '700': '#29ba74',
    '500': '#29ba74',
    A200: '#29ba74',
  }
)
const bcgWarn = Object.assign(
  { ...red },
  {
    '800': '#bf360c',
  }
)

export const bcg = createMuiTheme({
  palette: {
    primary: bcgPrimary,
    secondary: bcgSecondary,
    warn: bcgWarn,
  },
})

const goshabaPrimary = {
  '50': '#eef8f9',
  '100': '#ddf2f2',
  '200': '#bce5e5',
  '300': '#9ad7d8',
  '400': '#78cacb',
  '500': '#57bdbf',
  '600': '#469799',
  '700': '#447273',
  '800': '#234b4c',
  '900': '#112626',
}
const goshabaSecondary = grey
const goshabaWarn = red

export const goshaba = createMuiTheme({
  palette: {
    primary: goshabaPrimary,
    secondary: goshabaSecondary,
    warn: goshabaWarn,
  },
})
