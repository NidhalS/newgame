// Util file containing some function to download files.

/**
 * Create a .csv file and download it.
 *
 * @param {String} fileName name of the .csv file.
 * @param {Array} rows content of the .csv file. It is an array of Objetc. Each Object is a row.
 * @param {String} separator caracter used as seperator in the .csv file.
 * @param {String} lineBreak caracter used as line breaker in the .csv file.
 * @returns {Boolean} return the error if the creation/download didn't work.
 */
const csv = (
  fileName = `noName-${new Date().toLocaleDateString()}.csv`,
  rows,
  separator = ';',
  lineBreak = '\n'
) => {
  if (!rows.length) return 'Error: no rows'
  try {
    const csvContent =
      'data:text/csv;charset=utf-8,' +
      rows.map((e) => e.join(separator)).join(lineBreak)

    const encodedUri = encodeURI(csvContent)
    const link = document.createElement('a')
    link.setAttribute('href', encodedUri)
    link.setAttribute('download', fileName)
    document.body.appendChild(link) // Required for FF

    link.click()
    return false
  } catch (error) {
    return error
  }
}

export default {
  csv,
}
