import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import { bounceInDown } from '../../style'
import Card from '../Card'
import FabIcon from '../FabIcon'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`

const Content = styled(Card)`
  position: relative;
  width: 80%;
  padding: 24px;
  margin: ${({ hasLogo }) => (hasLogo ? '86px' : '24px')} auto 0;
  border-radius: 20px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
  font-size: 20px;
  text-align: center;
  ${bounceInDown};
`

const LogoWrapper = styled(Card)`
  position: absolute;
  top: -64px;
  left: 50%;
  transform: translateX(-50%);
  width: 90px;
  height: 90px;
  border-radius: 50%;
`

const Logo = styled.img`
  position: absolute;
  left: 15px;
  top: 13px;
  width: 60px;
`

const Children = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${({ hasLogo }) => hasLogo && '24px'};

  img {
    height: 30vh;
  }
`

/**
 * Tutorial shown at the beginning of each mini-game
 */
const GameTutorial = ({ logo, onClick, children }) => (
  <Wrapper>
    <Content hasLogo={!!logo}>
      {logo && (
        <LogoWrapper>
          <Logo src={logo} />
        </LogoWrapper>
      )}
      <Children hasLogo={!!logo}>{children}</Children>
    </Content>
    <FabIcon Icon={<PlayArrowIcon />} onClick={onClick} />
  </Wrapper>
)

GameTutorial.propTypes = {
  /**
   * SVG of the mini-game's logo
   */
  logo: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
}

export default GameTutorial
