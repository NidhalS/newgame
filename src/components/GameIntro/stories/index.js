import React from 'react'
import { action } from '@storybook/addon-actions'
import logo from '../../Memory/stories/logo.svg'

import GameIntro from '..'

export default {
  title: 'GameIntro',
  parameters: {
    component: GameIntro,
  },
}

export const main = () => <GameIntro logo={logo} onClick={action('onClick')} />
