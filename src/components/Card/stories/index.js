import React from 'react'
import styled from 'styled-components'
import { bounceInDown } from '../../../style'
import useRerender from '../../../stories/useRerender'

import Card from '..'

const MainCard = styled(Card)`
  width: 90%;
  margin: 40px auto;
`

const AnimatedCard = styled(MainCard)`
  ${bounceInDown}
  cursor: pointer;
`

const RoundCard = styled(MainCard)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 300px;
  height: 300px;
  border-radius: 50%;
  font-size: 48px;
`

export default {
  title: 'Card',
  parameters: {
    component: Card,
    notes: 'Basic reusable card',
  },
}

export const main = () => <MainCard>Basic card</MainCard>
export const animated = () => {
  // False positive error, used to allow the animation to repeat on click
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const props = useRerender()

  return (
    <AnimatedCard {...props}>
      Animated card <em>(click to restart animation)</em>
    </AnimatedCard>
  )
}
export const round = () => <RoundCard>Round card</RoundCard>
