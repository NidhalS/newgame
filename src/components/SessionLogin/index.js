import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ThemeProvider, StylesProvider } from '@material-ui/styles'
//import { IntlProvider } from 'react-intl'
import { GlobalStyle, themes } from '../../style'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import InputAdornment from '@material-ui/core/InputAdornment'
import Person from '@material-ui/icons/Person'
import BackgroundLayout from '../BackgroundLayout'
//import { FormattedMessage } from 'react-intl'

const LeftSide = styled.div`
  height: 100%;
  width: 50%;
  position: absolute;
  right: 0;
  top: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const WelcomText = styled.div`
  color: ${({ color }) => color};
  font-weight: bold;
  font-size: 20px;
  width: 100%;
  text-align: center;
  text-transform: uppercase;
`

const LoginTextField = styled(TextField)`
  fieldset {
    border-radius: 50px;
    background-color: white;
    border-color: transparent;
    z-index: 0;
  }
  input {
    z-index: 1;
  }
  svg {
    z-index: 1;
  }
  width: 50%;
  height: 56px;
  margin: 50px;
`

const LoginButton = styled(Button)`
  width: 50%;
  height: 56px;
  border-radius: 50px;
  border-width: 2px;
  background-color: white;
  &:hover {
    background-color: white;
  }
`

// Get all language files
const context = require.context(
  '../../../app/config/languages/',
  false,
  /\.js$/
)
const filenameRegexp = /^\.\/(.*)\.js$/
const languageFiles = context.keys().reduce((acc, filename) => {
  return {
    ...acc,
    [filename.match(filenameRegexp)[1]]: context(filename).default,
  }
}, {})

// TODO Refactor when apply-position switchs to React. Warning about theme props
const SessionLogin = ({
  backgroundImage,
  backgroundColor,
  onLogin,
  lang,
  theme,
}) => {
  const [candidateSpecialId, setCandidateSpecialId] = useState('')

  const handleLogin = () => {
    onLogin({
      candidateSpecialId,
    })
  }

  const handleChange = (event) => {
    setCandidateSpecialId(event.target.value)
  }

  return (
    // <IntlProvider locale={lang} messages={languageFiles[lang]}>
    <StylesProvider injectFirst>
      <GlobalStyle />
      <ThemeProvider theme={theme ? themes[theme] : themes['goshaba']}>
        <BackgroundLayout
          backgroundImg={backgroundImage}
          backgroundColor={backgroundColor}
        >
          <LeftSide>
            <WelcomText
              color={
                theme
                  ? themes[theme].palette.primary[500]
                  : themes['goshaba'].palette.primary[500]
              }
            >
              {/* <FormattedMessage id={'LOG_IN_TEXT'} /> */}
              {{ en: 'Please log in', fr: 'Veuillez vous connecter' }[lang]}
            </WelcomText>
            {/* <FormattedMessage id={'APPLICANT_ID'}>
                {(label) => ( */}
            <LoginTextField
              variant="outlined"
              placeholder={{ en: 'Applicant Id', fr: 'Identifiant' }[lang]}
              onChange={handleChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Person color="primary" />
                  </InputAdornment>
                ),
              }}
            />
            {/* )} */}
            {/* </FormattedMessage> */}
            <LoginButton
              variant="outlined"
              color="primary"
              onClick={handleLogin}
            >
              {/* <FormattedMessage id={'LOG_IN_BUTTON'} /> */}
              {{ en: 'Log in', fr: 'Se connecter' }[lang]}
            </LoginButton>
          </LeftSide>
        </BackgroundLayout>
      </ThemeProvider>
    </StylesProvider>
    // </IntlProvider>
  )
}

export default SessionLogin
