import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Paper from '@material-ui/core/Paper'
import SessionTableEditField from '../SessionTableEditField'
import SessionTableEditRow from '../SessionTableEditRow'
import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import SearchIcon from '@material-ui/icons/Search'
import AddIcon from '@material-ui/icons/Add'
import DownloadIcon from '@material-ui/icons/CloudDownload'
import companyApi from '../../domain/companyApi'
import bcgCustomScoring from '../../domain/bcgCustomScoring'
//import { FormattedMessage } from 'react-intl'
import SessionHeader from '../SessionHeader'
import download from '../../utils/download'

// ? Refactoring pass position list to this component. In fact, the positions are also used in SessionIpWhiteList (for instance) and the initiation is made twice
// TODO Refactoring standardize setState call (use of previousState, see handleSearch())
// TODO Error translations

const SessionTableContainer = styled(TableContainer)`
  padding: 20px;
`

const SessionTableHead = styled(TableHead)``

const IconCell = styled(TableCell)`
  display: flex;
  flex-direction: row;
  border: none;
  width: ${({ width }) => width};
`

const TableCellHeader = styled(TableCell)`
  border: none;
  width: ${({ width }) => width};
  padding: 3px;
`

const TableCellBody = styled(TableCell)`
  border: none;
  width: ${({ width }) => width};
  padding: 3px;
`

const Actions = styled.div``

const AddButton = styled(Button)`
  margin: 10px;
  box-shadow: none;
`

const ExtractButton = styled(Button)`
  margin: 10px;
  box-shadow: none;
`

const SessionTablePagination = styled(TablePagination)`
  border-top: 1px solid #dcdcdc;
`

const TableFilterRow = styled(TableRow)`
  box-shadow: ${({ shadow }) => shadow};
  border-top: ${({ border }) => border};
  border-bottom: ${({ border }) => border};
  border-radius: 4px;
`

// ! Session position can't have multiple steps.
const SessionTable = ({ lang }) => {
  const [error, setError] = useState()
  /**
   * Display time of errors
   */
  const ERROR_TIMEOUT = 3000

  const [companyId, setCompanyId] = useState()
  const [candidates, setCandidates] = useState([])
  /**
   * Definition of the table columns.
   * {
   *    addable: 'true if the field can be set by the user when a new row is added.
   *    align: 'left, center, right, justify'
   *    editable: 'true if the column is editable',
   *    field: 'Key of the column used in the candidates array',
   *    filterable: 'true if the column is filterable'
   *    format: 'For column with date type. Object used in toLocaleString() method'
   *    mandatory: 'When add or edit, the value of this column can't be undefined is mandatory is set at true'
   *    onCheck: 'Function call to check the value in the field. This fonction is async and returns a row Object'
   *    query: 'Value of the search query',
   *    values: 'For column with array type. Array of {'value': 'key value', 'text': 'displayed text for the value', 'disabled': 'true if disabled'}'
   *    title: 'Name of the column displayed in the table. Handle translations',
   *    type: 'Type of the column. ['array', 'date', 'string']',
   *    width: 'Width of the column',
   * }
   */
  const [columns, setColumns] = useState([
    {
      addable: true,
      align: 'center',
      editable: false,
      field: 'specialId',
      filterable: true,
      mandatory: true,
      /**
       * Check if candidate has already access or apply to some positions.
       *
       * @param {Object} candidate { specialId, ...other } row state before check
       */
      onCheck: async (candidate) => {
        const response = await companyApi.checkCandidate({
          specialId: candidate.specialId,
        })
        if (response.status === 200) {
          if (
            response.data &&
            (response.data.sessionAccess || response.data.positions)
          ) {
            // Update columns to disable positions
            setColumns((prevColumns) => {
              const data = [...prevColumns]
              const index = data.findIndex(
                (column) => column.field === 'positionId'
              )
              data[index].values = data[index].values.map((position) => {
                if (
                  [
                    ...response.data.sessionAccess,
                    ...response.data.positions,
                  ].includes(position.value)
                ) {
                  position.disabled = true
                } else {
                  position.disabled = false
                }
                return position
              })
              return data
            })
          }
          // This data will update the editRowContent
          return response.data
        } else if (response.status === 204) {
          // Status 204 is No Content. That means the specialId is not used for the company. So the function return an empty row with the specialId
          return candidate
        } else {
          setError({
            text: "Can't check specialId",
            afterError: () => setError(),
            timeout: ERROR_TIMEOUT,
          })
        }
      },
      query: '',
      title: { en: 'Id', fr: 'Id' },
      type: 'string',
      width: '10%',
    },
    {
      addable: true,
      align: 'center',
      editable: true,
      field: 'firstName',
      filterable: true,
      mandatory: false,
      query: '',
      title: { en: 'First name', fr: 'Prénom' },
      type: 'string',
      width: '15%',
    },
    {
      addable: true,
      align: 'center',
      editable: true,
      field: 'lastName',
      filterable: true,
      mandatory: false,
      query: '',
      title: { en: 'Last name', fr: 'Nom' },
      type: 'string',
      width: '15%',
    },
    {
      addable: true,
      align: 'center',
      editable: false,
      field: 'positionId',
      filterable: true,
      mandatory: true,
      query: '',
      values: [],
      title: { en: 'Position', fr: 'Position' },
      type: 'array',
      width: '15%',
    },
    {
      addable: true,
      align: 'center',
      editable: true,
      field: 'sessionDate',
      filterable: true,
      mandatory: false,
      query: '',
      title: { en: 'Date', fr: 'Date' },
      type: 'date',
      format: { day: 'numeric', month: 'short', year: 'numeric' },
      width: '15%',
    },
    {
      addable: false,
      align: 'center',
      editable: false,
      field: 'formulaire',
      filterable: true,
      query: '',
      title: { en: 'Connection', fr: 'Connexion' },
      type: 'date',
      format: { day: 'numeric', month: 'short', year: 'numeric' },
      width: '15%',
    },
    {
      addable: false,
      align: 'center',
      editable: false,
      field: 'startTimer',
      filterable: false,
      query: '',
      title: { en: 'Start', fr: 'Début' },
      type: 'date',
      format: {
        hour: '2-digit',
        minute: '2-digit',
        hour12: { en: true, fr: false }[lang],
      },
      width: '5%',
    },
    {
      addable: false,
      align: 'center',
      editable: false,
      field: 'apply',
      filterable: false,
      query: '',
      title: { en: 'End', fr: 'Fin' },
      type: 'date',
      format: {
        hour: '2-digit',
        minute: '2-digit',
        hour12: { en: true, fr: false }[lang],
      },
      width: '5%',
    },
  ])
  /**
   * ['asc', 'desc'] sorting direction
   */
  const [order, setOrder] = useState('asc')
  /**
   * Field used to sort the table
   */
  const [orderBy, setOrderBy] = useState('')
  /**
   * Array containing the selectionKey of the selected candidates
   */
  const [selected, setSelected] = useState([])
  /**
   * Key used to select a candidate
   */
  const selectionKey = (candidate) =>
    JSON.stringify(candidate._id) + JSON.stringify(candidate.positionId)
  /**
   * Page variables
   */
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)
  /**
   * Index of the row which is in editing mode
   */
  const [editRowIndex, setEditRowIndex] = useState(-1)
  /**
   * true is the add row is displayed
   */
  const [showAddRow, setShowAddRow] = useState(false)
  /**
   * true is the filter row is displayed
   */
  const [showFilterRow, setShowFilterRow] = useState(false)

  // Variable that maps a position id with its first step id. Used to create the candidate Status that store the sessionDate.
  const [mapPositionStep, setMapPositionStep] = useState({})

  useEffect(() => {
    const fetchData = async () => {
      const response = await companyApi.getSessionData()

      if (response.status === 200) {
        if (response.data && response.data.companyId) {
          setCompanyId(response.data.companyId)
        } else {
          setError({
            text: 'No company',
            afterError: () => setError(),
            timeout: ERROR_TIMEOUT,
          })
        }
        if (response.data && response.data.positions) {
          const positionsData = response.data.positions.map((position) => {
            if (position.steps[0]) {
              // ! First step only

              setMapPositionStep((prevMap) => {
                let data = { ...prevMap }
                data[position._id] = position.steps[0]._id
                return data
              })

              return {
                value: position._id,
                text:
                  position.translations &&
                  position.translations[lang] &&
                  position.translations[lang].name
                    ? position.translations[lang].name
                    : { en: 'No name', fr: 'Pas de nom' }[lang],
              }
            } else {
              setError({
                text: 'No step',
                afterError: () => setError(),
                timeout: ERROR_TIMEOUT,
              })
            }
          })

          // Fill the values of position column
          setColumns((prevColumns) => {
            const data = [...prevColumns]
            const index = data.findIndex(
              (column) => column.field === 'positionId'
            )
            data[index].values = positionsData
            return data
          })

          setCandidates(response.data.candidates)
        } else {
          setError({
            text: 'No data',
            afterError: () => setError(),
            timeout: ERROR_TIMEOUT,
          })
        }
      } else {
        setError({
          text: 'Bad response from server',
          afterError: () => setError(),
          timeout: ERROR_TIMEOUT,
        })
      }
    }

    fetchData()
  }, [])

  /**
   * Filter the candidates in order to search in the table
   * @param {Object} candidate
   * ! When the time is relevant, we need to be able to filter by time
   */
  const filterCandidates = (candidate) => {
    // Object containing the functions used to filter according to the column type
    const filterByType = {
      string: (column) =>
        candidate[column.field] &&
        candidate[column.field]
          .toLowerCase()
          .includes(column.query.toLowerCase()),
      array: (column) =>
        candidate[column.field] && candidate[column.field] === column.query,
      date: (column) =>
        candidate[column.field] &&
        new Date(candidate[column.field]).toLocaleString(
          undefined,
          column.format || {}
        ) ===
          new Date(column.query).toLocaleString(undefined, column.format || {}),
    }

    return (
      columns
        .map((column) => !column.query || filterByType[column.type](column))
        .filter(Boolean).length === columns.length
    )
  }

  /**
   * Update the columns query in order to perform a search
   * @param {String} field field name of a column
   * @param {String} query
   */
  const handleSearch = (field, query) => {
    setColumns((prevColumns) => {
      const data = [...prevColumns]
      const index = data.findIndex((column) => column.field === field)
      data[index].query = query
      return data
    })
  }

  /**
   * Sorting functions
   */
  const createSortHandler = (property) => (event) => {
    handleRequestSort(event, property)
  }

  const getSorting = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => desc(a, b, orderBy)
      : (a, b) => -desc(a, b, orderBy)
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const desc = (a, b, orderBy) => {
    if (!b[orderBy]) {
      return -1
    }
    if (!a[orderBy]) {
      return 1
    }
    if (b[orderBy] < a[orderBy]) {
      return -1
    }
    if (b[orderBy] > a[orderBy]) {
      return 1
    }
    return 0
  }

  const stableSort = (array, cmp) => {
    const stabilizedThis = array.map((el, index) => [el, index])
    stabilizedThis.sort((a, b) => {
      const order = cmp(a[0], b[0])
      if (order !== 0) return order
      return a[1] - b[1]
    })
    return stabilizedThis.map((el) => el[0])
  }

  /**
   * Selection functions
   */
  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = candidates
        .filter(filterCandidates)
        .map((n) => selectionKey(n))
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }
    setSelected(newSelected)
  }

  /**
   * Handle page functions
   */
  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  /**
   * Add a new candidate
   * @param {Object} newCandidate
   */
  const handleAddRow = async (newCandidate) => {
    const response = await companyApi.addCandidateSession({
      candidate: newCandidate,
      stepId: mapPositionStep[newCandidate.positionId]
        ? mapPositionStep[newCandidate.positionId]
        : undefined,
      companyId,
    })

    if (response.status === 201) {
      newCandidate._id = response.data.candidateId
      setCandidates([newCandidate, ...candidates])
      setShowAddRow(false)
    } else {
      setError({
        text: 'Candidate creation failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }

    resetDisabledPositions()
  }

  /**
   * Update a candidate
   * @param {Object} newCandidate new version of the candidate
   * @param {Number} index index in the candidates array of the edited candidate
   */
  const handleEditRow = async (newCandidate, candidateId) => {
    const index = candidates.findIndex(
      (candidate) => candidate._id === candidateId
    )

    const response = await companyApi.updateCandidateSession({
      candidate: Object.assign({ ...candidates[index] }, newCandidate),
    })

    if (response.status === 200) {
      setEditRowIndex(-1)
      const data = [...candidates]
      data[index] = newCandidate
      setCandidates(data)
    } else {
      setError({
        text: 'Candidate update failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }

    resetDisabledPositions()
  }

  /**
   * Delete a candidate
   * @param {Number} candidateId id of the deleted candidate
   */
  const handleDeleteRow = async (candidateId) => {
    const index = candidates.findIndex(
      (candidate) => candidate._id === candidateId
    )
    const response = await companyApi.deleteCandidatesSession({
      candidates: [candidates[index]],
    })

    if (response.status === 200) {
      let data = [...candidates]
      data.splice(index, 1)
      setCandidates(data)
    } else {
      setError({
        text: 'Candidate deletion failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }
  }

  /**
   * Delete all selected candidate
   */
  const handleSelectedDelete = async () => {
    const response = await companyApi.deleteCandidatesSession({
      candidates: candidates.filter((candidate) =>
        selected.includes(selectionKey(candidate))
      ),
    })

    if (response.status === 200) {
      setCandidates(
        candidates.filter(
          (candidate) => !selected.includes(selectionKey(candidate))
        )
      )
      setSelected([])
    } else {
      setError({
        text: 'Candidates deletion failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }
  }

  /**
   * Show error when there are empty fields. Used for EditRow.
   *
   * @param {Array} emptyFields
   */
  const handleEmptyField = (emptyFields) => {
    setError({
      text: `Missing: ${emptyFields
        .map((column) => column.title[lang].toLowerCase())
        .join(', ')}`,
      afterError: () => setError(),
      timeout: ERROR_TIMEOUT,
    })
  }

  /**
   * Reset columns query if filter is displayed and switch filterRow display.
   */
  const handleFilterClick = () => {
    if (showFilterRow) {
      setColumns((prevColumns) => {
        return prevColumns.map((column) => {
          const data = { ...column }
          data.query = ''
          return data
        })
      })
    }
    setShowFilterRow(!showFilterRow)
  }

  /**
   * Reset disabled positions. Used after edit or add
   */
  const resetDisabledPositions = () => {
    setColumns((prevColumns) => {
      const data = [...prevColumns]
      const index = data.findIndex((column) => column.field === 'positionId')
      data[index].values = data[index].values.map((position) => {
        position.disabled = false
        return position
      })
      return data
    })
  }

  /**
   * Get all score and generate a .csv file.
   * ! Custom feature for bcg.
   */
  const downloadScores = async () => {
    const scores = await bcgCustomScoring()
    const fileName = `scores-${new Date().toLocaleDateString()}.csv`

    const error = download.csv(fileName, scores)

    if (error) {
      console.log(error)
      setError({
        text: 'Download failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }
  }

  /**
   * Get all selected candidates in a .csv file and download it.
   *
   * TODO refactor translation
   */
  const downloadCandidates = () => {
    const fileName = `candidates-${new Date().toLocaleDateString()}.csv`

    if (!selected.length) {
      setError({
        text: 'You need to select some candidates to download the list.',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
      return
    }

    const fields =
      lang === 'fr'
        ? ['ID Candidat', 'Prénom', 'Nom']
        : ['Candidate ID', 'First Name', 'Last Name']

    const rows = [
      fields,
      ...selected.map((candidateKey) => {
        const candidate = candidates.find(
          (c) => selectionKey(c) === candidateKey
        )
        if (candidate) {
          return [candidate.specialId, candidate.firstName, candidate.lastName]
        }
      }),
    ].filter(Boolean)
    const error = download.csv(fileName, rows)

    if (error) {
      console.log(error)
      setError({
        text: 'Download failed',
        afterError: () => setError(),
        timeout: ERROR_TIMEOUT,
      })
    }
  }

  /**
   * SessionTable Header render function. Diplay the columns header, handle sorts, search, global selection
   * and global deletion.
   */
  const renderSessionTableHeader = () => (
    <SessionTableHead>
      <TableRow>
        <TableCellHeader padding="checkbox">
          <Checkbox
            indeterminate={
              selected.length > 0 && selected.length < candidates.length
            }
            checked={
              candidates.length > 0 && selected.length === candidates.length
            }
            onChange={handleSelectAllClick}
            inputProps={{ 'aria-label': 'select all candidate' }}
          />
        </TableCellHeader>
        {columns.map((column) => (
          <TableCellHeader
            key={column.field}
            sortDirection={orderBy === column.field ? order : false}
            align={column.align}
            width={column.width}
          >
            <TableSortLabel
              tabIndex="-1"
              active={orderBy === column.field}
              direction={orderBy === column.field ? order : 'asc'}
              onClick={createSortHandler(column.field)}
            >
              {column.title[lang].toUpperCase()}
            </TableSortLabel>
          </TableCellHeader>
        ))}
        <IconCell width={'5%'}>
          <IconButton
            style={{ marginLeft: '48px' }}
            onClick={handleSelectedDelete}
          >
            <DeleteIcon />
          </IconButton>
        </IconCell>
      </TableRow>
      <TableFilterRow
        shadow={showFilterRow ? '0 3px 6px 0 rgba(0, 0, 0, 0.16)' : 'none'}
        border={showFilterRow ? 'none' : '1px solid #dcdcdc'}
      >
        <TableCellHeader></TableCellHeader>
        {columns.map((column) =>
          column.filterable ? (
            <TableCellHeader key={'edit-' + column.field}>
              {showFilterRow && (
                <SessionTableEditField
                  column={column}
                  value={column.query}
                  onChange={handleSearch}
                  lang={lang}
                  isFilter={true}
                />
              )}
            </TableCellHeader>
          ) : (
            <TableCellHeader key={'blank-' + column.field}></TableCellHeader>
          )
        )}
        <IconCell width={'5%'}>
          <IconButton
            style={{ marginLeft: '48px' }}
            onClick={handleFilterClick}
          >
            <SearchIcon />
          </IconButton>
        </IconCell>
      </TableFilterRow>
    </SessionTableHead>
  )

  /**
   * SessionTable Body render function. Display the table content, filter and sort the wanted rows.
   * Enable row selection, edition and deletion.
   */
  const renderSessionTableBody = () => (
    <TableBody>
      {showAddRow && (
        <SessionTableEditRow
          columns={columns}
          onConfirm={handleAddRow}
          onClose={() => {
            setShowAddRow(false)
            resetDisabledPositions()
          }}
          onEmptyFieldError={handleEmptyField}
          row={columns.reduce((obj, column) => {
            obj[column.field] = ''
            return obj
          }, {})}
          lang={lang}
          prepend={1}
        />
      )}
      {stableSort(
        candidates.filter(filterCandidates),
        getSorting(order, orderBy)
      )
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row, index) => {
          const isItemSelected = selected.indexOf(selectionKey(row)) !== -1
          const labelId = `enhanced-table-checkbox-${index}`
          if (editRowIndex === index) {
            return (
              <SessionTableEditRow
                key={index}
                columns={columns}
                row={{ ...row }}
                onConfirm={(newCandidate) =>
                  handleEditRow(newCandidate, row._id)
                }
                onClose={() => {
                  setEditRowIndex(-1)
                  resetDisabledPositions()
                }}
                onEmptyFieldError={handleEmptyField}
                lang={lang}
                prepend={1}
              />
            )
          }
          return (
            <TableRow
              hover
              role="checkbox"
              aria-checked={isItemSelected}
              tabIndex={-1}
              key={index}
              selected={isItemSelected}
            >
              <TableCellBody
                padding="checkbox"
                onClick={(event) => handleClick(event, selectionKey(row))}
              >
                <Checkbox
                  checked={isItemSelected}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </TableCellBody>
              {columns.map((column) => {
                if (column.type === 'array') {
                  return (
                    <TableCellBody
                      align={column.align}
                      key={row.id + column.field}
                      onClick={(event) => handleClick(event, selectionKey(row))}
                      width={column.width}
                    >
                      {
                        column.values.find((c) => c.value === row[column.field])
                          .text
                      }
                    </TableCellBody>
                  )
                } else if (column.type === 'date') {
                  return (
                    <TableCellBody
                      align={column.align}
                      key={row.id + column.field}
                      onClick={(event) => handleClick(event, selectionKey(row))}
                      width={column.width}
                    >
                      {row[column.field]
                        ? new Date(row[column.field]).toLocaleString(
                            { en: 'en-US', fr: 'fr' }[lang],
                            column.format || {}
                          )
                        : ''}
                    </TableCellBody>
                  )
                } else if (column.type === 'string') {
                  return (
                    <TableCellBody
                      align={column.align}
                      key={row.id + column.field}
                      onClick={(event) => handleClick(event, selectionKey(row))}
                      width={column.width}
                    >
                      {row[column.field]}
                    </TableCellBody>
                  )
                }
              })}
              <IconCell width={'5%'}>
                <IconButton
                  onClick={() => {
                    setEditRowIndex(index)
                    setShowAddRow(false)
                  }}
                >
                  <EditIcon />
                </IconButton>
                <IconButton onClick={() => handleDeleteRow(row._id)}>
                  <DeleteIcon />
                </IconButton>
              </IconCell>
            </TableRow>
          )
        })}
    </TableBody>
  )

  return (
    <Paper>
      <SessionHeader
        title={{ en: 'Candidates list', fr: 'Liste de candidats' }[lang]} //'CANDIDATE_LIST'}
        Actions={() => (
          <Actions>
            {companyId === '5dee185b27a434064652ac9e' ? (
              <ExtractButton
                variant="outlined"
                color="primary"
                onClick={downloadScores}
                startIcon={<DownloadIcon />}
              >
                {/* <FormattedMessage id={'ADD_CANDIDATE'} /> */}
                {{ en: 'Download scores', fr: 'Télécharger les scores' }[lang]}
              </ExtractButton>
            ) : (
              <ExtractButton
                variant="outlined"
                color="primary"
                onClick={downloadCandidates}
                startIcon={<DownloadIcon />}
              >
                {/* <FormattedMessage id={'ADD_CANDIDATE'} /> */}
                {
                  {
                    en: 'Download .csv',
                    fr: 'Télécharger .csv',
                  }[lang]
                }
              </ExtractButton>
            )}
            <AddButton
              variant="contained"
              color="primary"
              onClick={() => {
                setShowAddRow(!showAddRow)
                setEditRowIndex(-1)
              }}
              startIcon={<AddIcon />}
            >
              {/* <FormattedMessage id={'ADD_CANDIDATE'} /> */}
              {{ en: 'Add a candidate', fr: 'Ajouter un candidat' }[lang]}
            </AddButton>
          </Actions>
        )}
        error={error}
      />
      <SessionTableContainer>
        <Table>
          {renderSessionTableHeader()}
          {renderSessionTableBody()}
        </Table>
      </SessionTableContainer>
      {/* <FormattedMessage id="ROWS_PER_PAGE"> */}
      {/* {(label) => ( */}
      <SessionTablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={candidates.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        // labelRowsPerPage={label}
        labelRowsPerPage={{ en: 'Rows per page', fr: 'Lignes par page' }[lang]}
      />
      {/* )} */}
      {/* </FormattedMessage> */}
    </Paper>
  )
}

export default SessionTable
