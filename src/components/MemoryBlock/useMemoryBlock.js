import { useState, useCallback, useEffect, useRef } from 'react'

export default ({ block, timeout, onAnswer, onFinish }) => {
  const [step, setStep] = useState(0)
  const [attemptIndex, setAttemptIndex] = useState(0)
  const timer = useRef(Date.now())

  const tutorial = block.tutorial[0]
  const currentTutorial = tutorial.tutoSteps && tutorial.tutoSteps[attemptIndex]

  const currentAttempt = block.attempts[attemptIndex]
  const isCurrentAttemptUseless =
    currentAttempt && currentAttempt.type === 'useless'
  const prevAttempt = block.attempts[attemptIndex - block.back]
  const isSameAsPrevLetter =
    prevAttempt && prevAttempt.letter === currentAttempt.letter
  const isLastAttempt = !block.attempts[attemptIndex + 1]

  const isTimerRunning = !isCurrentAttemptUseless && !currentTutorial

  const goToNextAttempt = useCallback(() => {
    if (!isLastAttempt) {
      setAttemptIndex(attemptIndex + 1)
    } else {
      setStep(0)
      setAttemptIndex(0)
      onFinish()
    }
  }, [attemptIndex, isLastAttempt, onFinish])

  const onClick = useCallback(
    (answerIndex) => {
      const answer = currentAttempt.choices[answerIndex].choice 
      const isAnswerCorrect = currentAttempt.choices[answerIndex].type === "true"

      onAnswer({
        answer: {
          ...currentAttempt,
          answer,
          time: (Date.now() - timer.current).toFixed(2),
        },
        isAnswerCorrect,
        attemptIndex,
      })
      goToNextAttempt()
    },
    [
      attemptIndex,
      currentAttempt,
      goToNextAttempt,
      isCurrentAttemptUseless,
      isSameAsPrevLetter,
      onAnswer,
    ],
  )

  // Will send an empty answer if the user doesn't answer in time
  useEffect(() => {
    timer.current = Date.now()
    if (step < 1 || !isTimerRunning) return

    const currentTimeout = setTimeout(() => {
      onAnswer({
        answer: {
          ...currentAttempt,
          time: timeout.toFixed(2),
        },
        isAnswerCorrect: false,
        attemptIndex,
      })
      goToNextAttempt()
    }, timeout)

    return () => clearTimeout(currentTimeout)
  }, [
    attemptIndex,
    currentAttempt,
    goToNextAttempt,
    isTimerRunning,
    onAnswer,
    step,
    timeout,
  ])

  return {
    isShowingTutorial: step === 0,
    tutorial,
    currentTutorial,
    onTutorialEnd: () => setStep(1),
    isShowingCountdown: step === 1,
    onCountdownEnd: () => setStep(2),
    currentAttempt,
    attemptIndex,
    isCurrentAttemptUseless,
    isTimerRunning,
    onClick,
  }
}
