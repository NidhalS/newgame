import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import { AnimatePresence } from 'framer-motion'
import styled from 'styled-components'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import ThumbDownIcon from '@material-ui/icons/ThumbDown'
import ThumbUpIcon from '@material-ui/icons/ThumbUp'
import GameTutorial from '../GameTutorial'
import GameCountdown from '../GameCountdown'
import GameBlockHeader from '../GameBlockHeader'
import Card from '../Card'
import FabIcon from '../FabIcon'
import useMemoryBlock from './useMemoryBlock'


const ImageWrapper = styled.div `
position: static;

`


const Button = styled.button`
  position: static;
  border: none;
  color: ${props=>props.Disabled ? "#666666" : "white"};
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius :12px;
  background-color:${props=>props.Enabled ? "blue" : props.Disabled ? "#cccccc" : "#4CAF50"};
  width :${props=>props.next ? "40%" :  "30%" };
  box-shadow: 0 2px #999;
 
  &:hover{
    background-color: ${props=>!props.next ? "blue" : ""};
  };
  &:active{
    box-shadow: 0 5px #666;
    transform: translateY(4px);
  };
  &:focus{
    background-color: blue;
  }
  
`
const ChoicesWrapper = styled.div`
margin-top : 30px;
margin-left: 50px;
display: inline-block;

`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`


const Rounds = styled.div`
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  margin: 12px;
`
const Round = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background: ${({ round }) => {
    if (round === 1) {
      return 'yellow'
    }
    if (round === 2) {
      return 'orange'
    }
    return 'red'
  }};
  opacity: ${({ isPast }) => (isPast ? 1 : 0.2)};
`



const TutorialText = styled(Card)`
  
  margin-top: 30px;
  bottom: 136px;
  left: ${({ orientation }) => {
    if (orientation === 'right') return 'auto'
    return '5%'
  }};
  right: ${({ orientation }) => {
    if (orientation === 'right') return '5%'
    return 'auto'
  }};
  width: 40%;
  font-size: 2.5vh;
  
`


const timeout = 4650

/**
 * Block of the Memory mini-game
 */

const MemoryBlock = ({
  block,
  rounds,
  currentRound,
  goodScore,
  badScore,
  lang,
  onAnswer,
  onFinish,
}) => {
  const {
    isShowingTutorial,
    tutorial,
    currentTutorial,
    onTutorialEnd,
    isShowingCountdown,
    onCountdownEnd,
    currentAttempt,
    attemptIndex,
    isCurrentAttemptUseless,
    isTimerRunning,
    onClick,
  } = useMemoryBlock({ block, timeout, onAnswer, onFinish })
  var [Disabled , setDisabled] = useState(true)
  
   var [Enabled,setEnabled] = useState(false)
   var [answerIndex,setanswerIndex] = useState(null) 
 
  // Helpers to use the keyboard as well as the mouse
  useEffect(() => {
    
   

    const onKeydown = (e) => {
      if (isShowingCountdown) return

      const isYes = e.code === 'Space' || e.code === 'KeyR'
      const isNo = e.code === 'KeyT'

      if (isShowingTutorial) {
        if (isYes) {
          onTutorialEnd()
        }
        return
      }

      if (isYes) {
        onClick(true)
      } else if (isNo) {
        onClick(false)
      }
    }

    window.addEventListener('keydown', onKeydown)
    return () => {
      window.removeEventListener('keydown', onKeydown)
    }
  }, [isShowingCountdown, isShowingTutorial, onTutorialEnd, onClick])

  if (isShowingTutorial) {
    return (
      <GameTutorial onClick={onTutorialEnd}>
        {tutorial.tops[lang]}
        <img src={tutorial.img} alt="memory" />
        {tutorial.bottoms[lang]}
      </GameTutorial>
    )
  }

  if (isShowingCountdown) {
    return <GameCountdown onFinish={onCountdownEnd} />
  }
  

  return (
    <Wrapper>
      <Rounds>
        {rounds.map((round, i) => (
          <Round key={i} round={round} isPast={i <= currentRound}>
            {round}
          </Round>
        ))}
      </Rounds>
      <GameBlockHeader
        key={attemptIndex}
        goodScore={goodScore}
        badScore={badScore}
        timeout={timeout}
        isTimerRunning={isTimerRunning}
      />
     <ImageWrapper>
     <img src={currentAttempt.img} alt="test" width="450px" height="300px" style={{borderRadius: "20px"}}/>
     </ImageWrapper>
     
    
      
      
      
      
      
      
        <TutorialText >
          {currentAttempt.question}
        </TutorialText>
      
      <ChoicesWrapper>
      {    
          currentAttempt.choices.map((j,i)=>{
  
          return(
            
            <Button onClick={() => {
              setanswerIndex(i)
              setDisabled(false)
              setEnabled(true)
              }}> 
              {j.choice}
            </Button>
          ) 
          })
        }
      </ChoicesWrapper>
        
    <Button 
    onClick={() => {
      onClick(answerIndex)
      setDisabled(true)
      setEnabled(false)
    }} Enabled={Enabled} Disabled={Disabled} disabled={Disabled} next>
      Continue
    </Button>
    </Wrapper>
  )
}

MemoryBlock.propTypes = {
  block: PropTypes.object,
  rounds: PropTypes.arrayOf(PropTypes.number).isRequired,
  currentRound: PropTypes.number.isRequired,
  goodScore: PropTypes.number.isRequired,
  badScore: PropTypes.number.isRequired,
  lang: PropTypes.string.isRequired,
  onAnswer: PropTypes.func.isRequired,
  onFinish: PropTypes.func.isRequired,
}

export default MemoryBlock
