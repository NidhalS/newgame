import React from 'react'
import { action } from '@storybook/addon-actions'
import styled from 'styled-components'
import useRerender from '../../../stories/useRerender'

import GameCountdown from '..'

const Wrapper = styled.div`
  height: 100%;
  min-height: 300px;
`

export default {
  title: 'GameCountdown',
  parameters: {
    component: GameCountdown,
  },
}

export const main = () => {
  // False positive error, used to allow the animation to repeat on click
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const props = useRerender()

  return (
    <Wrapper {...props}>
      <GameCountdown onFinish={action('onFinish')} />
    </Wrapper>
  )
}
