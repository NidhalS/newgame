import React from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'
import { useTheme } from '@material-ui/core/styles'

const StyledCard = styled(motion.div)`
  padding: 10px;
  background: ${({ theme }) => theme.palette.primary.main};
  border: 1px solid ${({ theme }) => theme.palette.primary.light};
  border-radius: 20px;
  text-align: center;
  color: ${({ theme }) => theme.palette.primary.contrastText};
`

const Card = (props) => {
  const theme = useTheme()

  return <StyledCard {...props} theme={theme} />
}

export default Card
