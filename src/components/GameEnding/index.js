import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { bounceInDown } from '../../style'
// import { FormattedMessage } from 'react-intl'
import { Fab } from '@material-ui/core'
import Card from '../Card'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
  min-height: 300px;
`

const Content = styled(Card)`
  align-self: center;
  width: 90%;
  margin-top: 24px;
  font-size: 30px;
  ${bounceInDown}
`

const Action = styled(Fab)`
  width: 300px;
  margin-bottom: 24px;
`

/**
 * Ending of a mini-game
 */
const GameEnding = ({ onClick }) => (
  <Wrapper>
    <Content>
      {/* <FormattedMessage id="END_GAME" /> */}
    </Content>
    <Action variant="extended" color="secondary" size="large" onClick={onClick}>
      {/* <FormattedMessage id="CONTINUE" /> */}
      CONTINUE
    </Action>
  </Wrapper>
)

GameEnding.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default GameEnding
