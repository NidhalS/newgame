import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Fab } from '@material-ui/core'
import { bounceInDown } from '../../style'
// import { FormattedMessage } from 'react-intl'
import Card from '../Card'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`

const LogoWrapper = styled(Card)`
  position: relative;
  display: flex;
  justify-content: center;
  margin-top: 10vh;
  width: 40vh;
  height: 40vh;
  border-radius: 50%;
  ${bounceInDown};
`

const Logo = styled.img`
  width: 30vh;
`

const Action = styled(Fab)`
  width: 300px;
  margin-bottom: 24px;
`

/**
 * Introduction to a mini-game
 */
const GameIntro = ({ logo, onClick }) => (
  <Wrapper>
    <LogoWrapper>
      <Logo src={logo} />
    </LogoWrapper>
    <Action variant="extended" color="secondary" size="large" onClick={onClick}>
      {/* <FormattedMessage id="PLAY" /> */}
      PLAY
    </Action>
  </Wrapper>
)

GameIntro.propTypes = {
  /**
   * SVG of the mini-game's logo
   */
  logo: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default GameIntro
