import api from '../../app/shared/constants/api'

// api = {
//   "url": "http://localhost:3002",
//   "urlPreview": "http://localhost:8090",
//   "card": "http://localhost:3005/apicard",
//   "services": "http://localhost:8090"
// }

console.log(api)
export const getMinigameOrQuestionnaireData = async ({
  positionId,
  stepId,
}) => {
  const res = await fetch(
    `${api.url}/candidate/minigameOrQuestionnaire/next/${positionId}/${stepId}`,
  )
  return await res.json()
}
