import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import SessionTableEditField from '../SessionTableEditField'
import DoneIcon from '@material-ui/icons/Done'
import CloseIcon from '@material-ui/icons/Close'
import { IconButton } from '@material-ui/core'
import { onErrorResumeNext } from 'rxjs/operator/onErrorResumeNext'

const IconCell = styled(TableCell)`
  display: flex;
  flex-direction: row;
  border: none;
`

const TableCellEdit = styled(TableCell)`
  border: none;
  padding: 3px;
`

const SessionTableEditRow = ({
  columns,
  row,
  onConfirm,
  onClose,
  onEmptyFieldError,
  lang,
  prepend = 0,
  append = 0,
}) => {
  const [editRowContent, setEditRowContent] = useState({ ...row })

  const handleCheck = async (column) => {
    const data = await column.onCheck(editRowContent)

    if (data) {
      setEditRowContent(data)
    } else {
      // TODO add onError props
      console.log('Check failed')
    }
  }

  const handleChange = (field, value) => {
    setEditRowContent((oldRow) => {
      let data = { ...oldRow }
      data[field] = value
      return data
    })
  }

  const handleClick = () => {
    const emptyMandatoryFields = columns.filter(
      (column) => column.mandatory && !editRowContent[column.field]
    )

    if (emptyMandatoryFields.length === 0) {
      onConfirm(editRowContent)
    } else {
      onEmptyFieldError(emptyMandatoryFields)
    }
  }

  return (
    <TableRow>
      {Array(prepend)
        .fill(0)
        .map((index) => (
          <TableCellEdit key={'prepend-' + index}></TableCellEdit>
        ))}
      {columns.map((column) => {
        if (column.editable || (!row._id && column.addable)) {
          return (
            <TableCellEdit align={'center'} key={column.field}>
              <SessionTableEditField
                column={column}
                value={editRowContent[column.field]}
                onCheck={(value) => {
                  if (column.onCheck) return handleCheck(column, value)
                }}
                onChange={handleChange}
                lang={lang}
              />
            </TableCellEdit>
          )
        } else {
          return <TableCellEdit key={column.field}></TableCellEdit>
        }
      })}
      <IconCell>
        <IconButton onClick={handleClick}>
          <DoneIcon />
        </IconButton>
        <IconButton onClick={onClose}>
          <CloseIcon />
        </IconButton>
      </IconCell>
      {Array(append)
        .fill(0)
        .map((index) => (
          <TableCellEdit key={'append-' + index}></TableCellEdit>
        ))}
    </TableRow>
  )
}

SessionTableEditRow.propTypes = {
  /**
   * Column data: editable, field, title and type are used.
   */
  columns: PropTypes.array.isRequired,
  /**
   * Row data used to set default value.
   */
  row: PropTypes.object,
  /**
   * Function called when doneButton is clicked.
   */
  onConfirm: PropTypes.func.isRequired,
  /**
   * Function called when closeButton is clicked.
   */
  onClose: PropTypes.func.isRequired,
  /**
   * Function called when doneButton is clicked and empty field is mandatory.
   */
  onEmptyFieldError: PropTypes.func.isRequired,
  /**
   * Used to translate column title.
   */
  lang: PropTypes.string.isRequired,
  /**
   * Number of empty cells on the left
   */
  prepend: PropTypes.number,
  /**
   * Number of empty cells on the right
   */
  append: PropTypes.number,
}

export default SessionTableEditRow
