import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'
import { useTheme } from '@material-ui/core'

export const countDown = keyframes`
  0%, 100% {
    opacity: 0;
  }
  50% {
    opacity: 1;
    transform: translate(-50%, -50%) scale(1.5);
  }
`

const Step = styled.div`
  position: absolute;
  top: 45%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 60px;
  font-weight: 400;
  text-align: center;
  color: ${({ theme }) => theme.palette.primary.contrastText};
  text-shadow: ${({ theme }) => {
    const color = theme.palette.primary.main
    return `2px 2px 0 ${color}, 1px 0 0 ${color}, -1px 0 0 ${color}, 0 1px 0 ${color}, 0 -1px 0 ${color}`
  }};
  animation: 1s ${countDown} both;
`

const defaultSteps = ['3', '2', '1', 'Partez!']

/**
 * Countdown that triggers an event after 4 seconds
 */
const GameCountdown = ({ steps = defaultSteps, onFinish }) => {
  const theme = useTheme()
  const [step, setStep] = useState(0)

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (steps[step + 1]) {
        setStep(step + 1)
      } else {
        onFinish()
      }
    }, 1e3)

    return () => clearTimeout(timeout)
  }, [onFinish, step, steps])

  const currentStep = steps[step]

  return (
    <Step key={currentStep} theme={theme}>
      {currentStep}
    </Step>
  )
}

GameCountdown.propTypes = {
  /**
   * Array of strings that will be shown to the user every 1 second
   */
  steps: PropTypes.arrayOf(PropTypes.string),
  /**
   * Action triggered when all the steps where shown (after `steps.length` seconds)
   */
  onFinish: PropTypes.func.isRequired,
}

export default GameCountdown
