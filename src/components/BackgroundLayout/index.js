import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Bg = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: ${({ backgroundColor }) => backgroundColor}
    url(${({ backgroundImg }) => backgroundImg}) 50% 100% / 100% no-repeat;
`
//On Firefox, background-size should be after background-position, not after background-repeat. No problem on Chrome.

const Content = styled.main`
  position: relative;
  display: flex;
  flex-direction: column;
  height: 100vh;
`

/**
 * Layout of a game with the background with a wrapper to have the application full height with `display: flex` & `flex-direction: column`
 */
const BackgroundLayout = ({ backgroundImg, backgroundColor, children }) => {
  return (
    <Fragment>
      <Bg backgroundImg={backgroundImg} backgroundColor={backgroundColor} />
      <Content>{children}</Content>
    </Fragment>
  )
}

BackgroundLayout.propTypes = {
  /**
   * Data coming from the Angular app
   */
  backgroundImg: PropTypes.string.isRequired,
  /**
   * Data coming from the Angular app
   */
  backgroundColor: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}

export default BackgroundLayout
