import React, { useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
// import { FormattedMessage } from 'react-intl'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 30px 20px;
`

const ErrorWrapper = styled.div`
  margin: 10px;
  color: red;
`

const HeaderTitle = styled.div`
  color: #616161;
  font-size: 2em;
`

const SessionHeader = ({ title, error, Actions }) => {
  useEffect(() => {
    if (error) setTimeout(error.afterError, error.timeout)
  }, [error])

  return (
    <Wrapper>
      <HeaderTitle>
        {/* <FormattedMessage id={title} /> */}
        {title}
      </HeaderTitle>
      {error && <ErrorWrapper>{error.text}</ErrorWrapper>}
      <Actions />
    </Wrapper>
  )
}

SessionHeader.propTypes = {
  /**
   * Title displayed on the header
   */
  title: PropTypes.string.isRequired,
  /**
   * Component that is displayed on the left of the header
   */
  Actions: PropTypes.func.isRequired,
  /**
   * Text of the error
   */
  error: PropTypes.object,
}

export default SessionHeader
