import React from 'react'
import { withKnobs, number, boolean } from '@storybook/addon-knobs'

import GameBlockHeader from '..'

export default {
  title: 'GameBlockHeader',
  decorators: [withKnobs],
  parameters: {
    component: GameBlockHeader,
  },
}

const scoreOptions = {
  range: true,
  min: 0,
  max: 1,
  step: 0.1,
}

export const main = () => (
  <GameBlockHeader
    goodScore={number('goodScore', 0.75, scoreOptions)}
    badScore={number('badScore', 0.25, scoreOptions)}
    timeout={number('timeout', 3000)}
    isTimerRunning={boolean('isTimerRunning', true)}
  />
)
