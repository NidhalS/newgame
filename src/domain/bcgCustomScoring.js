import api from '../../app/shared/constants/api'
import axios from 'axios'

const filterAndSumByAxe = (array, selectedQuestions, multiQuestions, mode) => {
  return (
    array
      // * .map instead of .filter because we need the question index in the .reduce
      .map((question, index) => {
        if (selectedQuestions.includes(index + 1)) {
          return question
        } else {
          return null
        }
      })
      .reduce((acc, curr, index) => {
        if (!curr) {
          return acc
        }
        // No answer
        if (
          curr === null ||
          curr.filter((answer) => answer === null).length === curr.length
        ) {
          return acc
          // At least one wrong answer
        } else if (
          curr.filter((answer) => answer === false).length &&
          mode === 'A'
        ) {
          return acc
          // Return the score for B is answer
        } else if (mode === 'B') {
          return (
            acc +
            curr.filter((answer) => answer === false).length * -1 +
            curr.filter((answer) => answer === true).length *
              (multiQuestions.includes(index + 1) ? 2 : 3)
          )
          // Count the number of right answer
        } else {
          return (
            acc +
            (multiQuestions.includes(index + 1) ? 2 : 3) *
              curr.filter(Boolean).length
          )
        }
      }, 0)
  )
}

const groupByCandidate = (items) =>
  items.reduce((acc, curr) => {
    const index = acc.findIndex(
      (r) =>
        curr.candidate && r.candidate && r.candidate._id === curr.candidate._id
    )
    if (index === -1) {
      const tmp = {
        candidate: curr.candidate,
        position: curr.position,
        results: [
          {
            questionnaire: curr.questionnaire,
            questions: curr.questions,
          },
        ],
      }
      return [...acc, tmp]
    } else {
      const tmp = [...acc]
      tmp[index].results.push({
        questionnaire: curr.questionnaire,
        questions: curr.questions,
      })
      return tmp
    }
  }, [])

/**
 * Generate the data needed to download the .csv file containing the score of all candidates.
 */
export default async () => {
  const fields = [
    'Candidate ID',
    'First Name',
    'Last Name',
    'Global score (Option A)',
    'Business judgment (Option A)',
    'Equations (Option A)',
    'Logic (Option A)',
    'Quantitative (Option A)',
    'Global score (Option B)',
    'Business judgment (Option B)',
    'Equations (Option B)',
    'Logic (Option B)',
    'Quantitative (Option B)',
    'Feedback',
    'Feedback text',
  ]

  const mapQuestions = {
    'Global score': [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
    ],
    'Business judgment': [5, 6, 11, 12, 19, 25],
    Equations: [3, 4, 7, 17, 18, 23, 24],
    Logic: [8, 13, 15, 16, 20, 21, 22],
    Quantitative: [1, 2, 3, 4, 6, 7, 8, 9, 10, 14, 17, 18, 24],
  }

  const multiQuestions = [5, 8, 12, 16, 25]

  const response = await axios.get(`${api.url}/company/getAllResultsBcg`, {
    headers: {
      Authorization: `Bearer ${window.localStorage.satellizer_token}`,
    },
  })

  if (response.status === 200 && response.data) {
    const groupedResults = groupByCandidate(response.data)
    return [
      fields,
      ...groupedResults.map((result) => {
        if (result.candidate) {
          const indexFeedback = result.results.findIndex(
            (r) => r.questionnaire === '5e417368ac584a001ac5c825'
          )
          return [
            result.candidate.specialId,
            result.candidate.firstName,
            result.candidate.lastName,
            ...Object.keys(mapQuestions).map((axe) => {
              const index = result.results.findIndex(
                (r) => r.questions.length > 5
              )
              if (index !== -1) {
                return filterAndSumByAxe(
                  result.results[index].questions.filter(Boolean),
                  mapQuestions[axe],
                  multiQuestions,
                  'A'
                )
              }
            }),
            ...Object.keys(mapQuestions).map((axe) => {
              const index = result.results.findIndex(
                (r) => r.questions.length > 5
              )
              if (index !== -1) {
                return filterAndSumByAxe(
                  result.results[index].questions.filter(Boolean),
                  mapQuestions[axe],
                  multiQuestions,
                  'B'
                )
              }
            }),
            indexFeedback !== -1 &&
            result.results[indexFeedback].questions[0] &&
            result.results[indexFeedback].questions[0].find(Boolean)
              ? result.results[indexFeedback].questions[0].find(Boolean)
                  .translations.en.description
              : '',
            indexFeedback !== -1
              ? result.results[indexFeedback].questions[1]
              : '',
          ]
        }
      }),
    ].filter(Boolean)
  }
}
