import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { CircularProgress } from '@material-ui/core'
import TimerIcon from '@material-ui/icons/Timer'

const Wrapper = styled.div`
  position: relative;
  width: 80px;
  height: 80px;

  .MuiCircularProgress-circleStatic {
    transition: none;
  }
`

const Icon = styled(TimerIcon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 50px;
  color: white;
`

/**
 * Timer using material-ui `<CircularProgress>`<br />
 * See https://material-ui.com/api/circular-progress/ for more details
 */
const GameTimer = ({ timeout, isRunning = true }) => {
  const progressRef = useRef()

  useEffect(() => {
    if (!isRunning) return

    const circle = progressRef.current.querySelector(
      '.MuiCircularProgress-circle',
    )
    const strokeDasharray = parseFloat(circle.style.strokeDasharray)

    let raf
    let start = Date.now()
    const tick = () => {
      const time = Math.min(1, (Date.now() - start) / timeout)
      circle.style.strokeDashoffset = time * -strokeDasharray
      loop()
    }
    const loop = () => {
      raf = requestAnimationFrame(tick)
    }
    loop()

    return () => cancelAnimationFrame(raf)
  }, [isRunning, timeout])

  return (
    <Wrapper>
      <CircularProgress
        ref={progressRef}
        thickness={4}
        size={80}
        variant="static"
      />
      <Icon />
    </Wrapper>
  )
}

GameTimer.propTypes = {
  /**
   * How long does it take for the animation to finish which is the time it takes to answer the question
   */
  timeout: PropTypes.number.isRequired,
  isRunning: PropTypes.bool,
}

export default GameTimer
