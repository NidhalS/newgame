import React from 'react'
import styled from 'styled-components'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import ThumbDownIcon from '@material-ui/icons/ThumbDown'

import FabIcon from '..'

const StyledFab = styled(FabIcon)`
  display: block;
  margin: 40px auto;
`

export default {
  title: 'FabIcon',
  parameters: {
    component: FabIcon,
  },
}

export const main = () => <StyledFab Icon={<PlayArrowIcon />} />

export const primary = () => (
  <StyledFab Icon={<ThumbDownIcon />} color="primary" />
)
primary.story = {
  parameters: {
    notes: 'Secondary is the default color',
  },
}
