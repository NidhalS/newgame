import React from 'react';

import './App.css';
import logo from '../src/components/Memory/stories/logo.svg'
import GameIntro from '../src/components/GameIntro/index'
import Memory from '../src/components/Memory/index'
import data from '../src/components/Memory/stories/data.json'
import { action } from '@storybook/addon-actions'

function App() {
  return (
    <div>
      <Memory 
      d={data}
      position={{
      company: {
        colorBack: '#908787',
        gamePicture:
          'https://s3-eu-west-1.amazonaws.com/rhtool/image/BG_detective.png',
      },
    }}
    lang = "fr"
    onSendDataAndGetResult={action('onSendDataAndGetResult')}
    onEndGame={action('onEndGame')}
    />
    
    </div>
  );
}

export default App;
