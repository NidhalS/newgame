import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import SessionHeader from '../SessionHeader'
import AddIcon from '@material-ui/icons/Add'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { IconButton } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import SessionTableEditRow from '../SessionTableEditRow'
import companyApi from '../../domain/companyApi'
import Button from '@material-ui/core/Button'

// TODO Error translations

const AddButton = styled(Button)`
  margin: 10px;
  box-shadow: none;
`

const SessionIpWhiteList = ({ lang }) => {
  const [error, setError] = useState()
  // ? Better loading handler ?
  const [loading, setLoading] = useState(true)
  const [showAddRow, setShowAddRow] = useState(false)
  const [ipAddresses, setIpAddresses] = useState([])
  const [columns, setColumns] = useState([
    {
      field: 'ip',
      title: { en: 'Ip address', fr: 'Adresse ip' },
      type: 'string',
      addable: true,
    },
    {
      addable: true,
      field: 'positionId',
      values: [],
      title: { en: 'Position', fr: 'Position' },
      type: 'array',
    },
  ])

  useEffect(() => {
    const fetchData = async () => {
      const response = await companyApi.getAllPositionsInfo()

      if (response.status === 200) {
        if (response.data && response.data.length) {
          const positionsData = response.data.map((position) => {
            return {
              value: position._id,
              text:
                position.translations &&
                position.translations[lang] &&
                position.translations[lang].name
                  ? position.translations[lang].name
                  : { en: 'No name', fr: 'Pas de nom' }[lang],
            }
          })

          // Fill the values of position column
          setColumns((prevColumns) => {
            const data = [...prevColumns]
            const index = data.findIndex(
              (column) => column.field === 'positionId'
            )
            data[index].values = positionsData
            return data
          })

          // Unwind the ipAddresses
          setIpAddresses(
            response.data.reduce((acc, curr) => {
              return curr.ipWhiteList
                ? [
                    ...acc,
                    ...curr.ipWhiteList.map((ip) => ({
                      ip,
                      positionId: curr._id,
                    })),
                  ]
                : acc
            }, [])
          )
        } else {
          setError({
            text: 'No data',
            afterError: () => setError(),
            timeout: 3000,
          })
        }
      } else {
        setError({
          text: 'Bad response from server',
          afterError: () => setError(),
          timeout: 3000,
        })
      }
    }

    fetchData().then(() => setLoading(false))
  }, [])

  /**
   * Add a new ip address in a position
   * @param {Object} newIpAddress
   */
  const handleAddIp = async (newIpAddress) => {
    const response = await companyApi.addIpWhiteList(newIpAddress)

    if (response.status === 200) {
      setIpAddresses([newIpAddress, ...ipAddresses])
      setShowAddRow(false)
    } else {
      setError({
        text: 'Ip addition failed',
        afterError: () => setError(),
        timeout: 3000,
      })
    }
  }

  /**
   * Delete a ip address in a position
   * @param {Object} ipAddress
   */
  const handleDeleteIp = async (ipAddress) => {
    const response = await companyApi.deleteIpWhiteList(ipAddress)

    if (response.status === 200) {
      const index = ipAddresses.findIndex((ip) => ip === ipAddress)
      let data = [...ipAddresses]
      data.splice(index, 1)
      setIpAddresses(data)
    } else {
      setError({
        text: 'Ip deleteion failed',
        afterError: () => setError(),
        timeout: 3000,
      })
    }
  }

  return (
    <Paper>
      <SessionHeader
        title={{ en: 'Ip list', fr: 'Liste des sites' }[lang]} //'IP_WHITE_LIST'}
        Actions={() => (
          <AddButton
            variant="contained"
            color="primary"
            onClick={() => {
              setShowAddRow(!showAddRow)
            }}
            startIcon={<AddIcon />}
          >
            {/* <FormattedMessage id={'ADD_IP'} /> */}
            {{ en: 'Add an ip', fr: 'Ajouter une ip' }[lang]}
          </AddButton>
        )}
        error={error}
      />
      {loading ? (
        <div>Loading...</div>
      ) : (
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell key={column.field} align={'center'}>
                    {column.title[lang].toUpperCase()}
                  </TableCell>
                ))}
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {showAddRow && (
                <SessionTableEditRow
                  columns={columns}
                  onConfirm={handleAddIp}
                  onClose={() => setShowAddRow(false)}
                  onEmptyFieldError={() => {}}
                  row={columns.reduce((obj, column) => {
                    obj[column.field] = ''
                    return obj
                  }, {})}
                  lang={lang}
                />
              )}
              {ipAddresses.map((row, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell align={'center'}>{row.ip}</TableCell>
                    <TableCell align={'center'}>
                      {
                        columns
                          .find((column) => column.field === 'positionId')
                          .values.find((position) => {
                            return position.value === row.positionId
                          }).text
                      }
                    </TableCell>
                    <TableCell>
                      <IconButton onClick={() => handleDeleteIp(row)}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Paper>
  )
}

export default SessionIpWhiteList
