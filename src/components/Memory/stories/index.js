import React from 'react'
import { action } from '@storybook/addon-actions'

import Memory from '..'
import data from './data.json'

export default {
  title: 'Mini-game|Memory',
  parameters: {
    component: Memory,
  },
}

export const main = () => (
  <Memory
    d={data}
    position={{
      company: {
        colorBack: '#908787',
        gamePicture:
          'https://s3-eu-west-1.amazonaws.com/rhtool/image/BG_detective.png',
      },
    }}
    lang="fr"
    onSendDataAndGetResult={action('onSendDataAndGetResult')}
    onEndGame={action('onEndGame')}
  />
)
