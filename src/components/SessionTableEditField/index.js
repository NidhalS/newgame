import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import DateFnsUtils from '@date-io/date-fns'
import frLocale from 'date-fns/locale/fr'
import enLocale from 'date-fns/locale/en-US'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers'
import FormControl from '@material-ui/core/FormControl'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import InputAdornment from '@material-ui/core/InputAdornment'
import Search from '@material-ui/icons/Search'
import InputLabel from '@material-ui/core/InputLabel'

const SearchIcon = styled(Search)`
  color: #6d6d6d;
`

const ArrayField = styled(FormControl)`
  width: 100%;
`

const initByType = {
  array: '',
  date: null,
  string: '',
}

const SessionTableEditField = ({
  column,
  onCheck = () => {},
  onChange,
  value,
  lang,
  isFilter,
}) => {
  const [fieldValue, setFieldValue] = useState(value || initByType[column.type])

  //Need to set a label on ArrayField
  const inputLabel = useRef(null)
  const [labelWidth, setLabelWidth] = useState(0)
  useEffect(() => {
    if (column.type === 'array') {
      setLabelWidth(inputLabel.current.offsetWidth)
    }
  }, [])

  const localeMap = {
    en: enLocale,
    fr: frLocale,
  }

  // TODO Remove data of the array that are not available
  useEffect(() => {
    if (value) {
      setFieldValue(value)
    }
  }, [value])

  const handleChange = (e) => {
    setFieldValue(e.target.value)
    onChange(column.field, e.target.value)
  }

  const handleDateChange = (date) => {
    setFieldValue(date)
    onChange(column.field, date)
  }

  const handleBlur = (e) => {
    if (e.target.value) onCheck(e.target.value)
  }

  if (column.type == 'array') {
    return (
      <ArrayField variant="outlined">
        <InputLabel ref={inputLabel} id="array-field-label">
          {isFilter
            ? undefined
            : column.mandatory
            ? '* ' + column.title[lang]
            : column.title[lang]}
        </InputLabel>
        <Select
          labelId="array-field-label"
          labelWidth={labelWidth}
          value={fieldValue}
          onBlur={handleBlur}
          onChange={handleChange}
          variant="outlined"
        >
          {[
            { value: '', text: { en: '...', fr: '...' }[lang] },
            ...column.values,
          ].map((v) => (
            <MenuItem
              disabled={!isFilter && v.disabled}
              key={v.value}
              value={v.value}
            >
              {v.text}
            </MenuItem>
          ))}
        </Select>
      </ArrayField>
    )
  } else if (column.type == 'date') {
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={localeMap[lang]}>
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          inputVariant="outlined"
          format="dd/MM/yyyy"
          label={
            isFilter
              ? undefined
              : column.mandatory
              ? '* ' + column.title[lang]
              : column.title[lang]
          }
          value={fieldValue}
          onBlur={handleBlur}
          onChange={handleDateChange}
        />
      </MuiPickersUtilsProvider>
    )
  }
  if (column.type == 'string') {
    return (
      <TextField
        label={
          isFilter
            ? undefined
            : column.mandatory
            ? '* ' + column.title[lang]
            : column.title[lang]
        }
        value={fieldValue}
        onBlur={handleBlur}
        onChange={handleChange}
        variant="outlined"
        fullWidth={true}
        InputProps={{
          endAdornment: isFilter ? (
            <InputAdornment>
              <SearchIcon />
            </InputAdornment>
          ) : (
            ''
          ),
        }}
      />
    )
  }
}

SessionTableEditField.propTypes = {
  /**
   * Column data: field, title and type are used.
   */
  column: PropTypes.object.isRequired,
  /**
   * If true the edit field will be display as a filter.
   */
  isFilter: PropTypes.bool,
  /**
   * Used to translate column title.
   */
  lang: PropTypes.string.isRequired,
  /**
   * Function called when the focus on the field is lost
   */
  onCheck: PropTypes.func,
  /**
   * Function called when the value in the field changes.
   */
  onChange: PropTypes.func.isRequired,
  /**
   * Default value.
   */
  value: PropTypes.oneOfType(PropTypes.string, PropTypes.date),
}

export default SessionTableEditField
