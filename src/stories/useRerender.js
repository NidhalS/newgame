import { useState } from 'react'

export default () => {
  const [key, setKey] = useState(0)

  return {
    key,
    onClick: () => setKey(key + 1),
    title: 'Click to restart animation',
    style: {
      cursor: 'pointer',
    },
  }
}
