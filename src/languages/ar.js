export default {
  'FRENCH': 'الفرنسية',
  'ENGLISH': 'الإنجليزية',
  'CHINESE': 'الصينية',
  'ARABIC': 'العربية'
};
