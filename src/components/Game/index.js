import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import GameIntro from '../GameIntro'
import GameTutorial from '../GameTutorial'
import GameEnding from '../GameEnding'
import useScore from '../../hooks/useScore'

/**
 * Reusable game component for every mini-game
 */
const Game = ({ data, logo, lang, onFinishGame, onNextGame, Block }) => {
  // The data sent to the backend is the same as the data we get from it but with additional data
  // Since we don't want to mutate the data we're duplicating them
  const [blocks, setBlocks] = useState(data.blocks)
  const [step, setStep] = useState(0)

  const nbQuestions = data.blocks.reduce(
    (acc, b) => acc + b.attempts.filter((a) => a.type !== 'useless').length,
    0,
  )
  
  const { score: goodScore, onAnswer: onGoodAnswer } = useScore({
    step: 1 / nbQuestions,
  })

  const { score: badScore, onAnswer: onBadAnswer } = useScore({
    step: 1 / nbQuestions,
  })

  const isIntro = step === 0
  const isTutorial = step === 1
  const blockIndex = step - 2
  const isEnding = !isIntro && !isTutorial && !data.blocks[blockIndex]

  useEffect(() => {
    if (!isEnding) return

    onFinishGame({
      data: blocks,
      score: {
        type: data.type,
      },
    })
  }, [blocks, data.type, isEnding, onFinishGame])

  if (isIntro) {
    return <GameIntro logo={logo} onClick={() => setStep(1)} />
  }

  if (isTutorial) {
    return (
      <GameTutorial logo={logo} onClick={() => setStep(2)}>
        {data.intros[lang]}
      </GameTutorial>
    )
  }

  if (isEnding) {
    return <GameEnding onClick={onNextGame} />
  }

  const block = data.blocks[blockIndex]

  const onAnswer = ({ answer, isAnswerCorrect, attemptIndex }) => {
    if (isAnswerCorrect) {
      onGoodAnswer()
    }
    // `isAnswerCorrect` can be null
    else if (isAnswerCorrect === false) {
      onBadAnswer()
    }

    setBlocks(
      blocks.map((block, i) => {
        if (i !== blockIndex) return block
        return {
          ...block,
          attempts: block.attempts.map((attempt, i) => {
            return i === attemptIndex ? answer : attempt
          }),
        }
      }),
    )
  }

  return (
    <Block
      block={block}
      rounds={data.blocks.map((d) => d.back)}
      currentRound={blockIndex}
      goodScore={goodScore}
      badScore={badScore}
      lang={lang}
      onAnswer={onAnswer}
      onFinish={() => setStep((step) => step + 1)}
    />
  )
}

Game.propTypes = {
  /**
   * Data of the mini-game, coming from the Angular app
   */
  data: PropTypes.object,
  /**
   * SVG of the mini-game's logo used for the intro & tutorial
   */
  logo: PropTypes.string.isRequired,
  /**
   * Language of the user, will use the translations in `data`
   */
  lang: PropTypes.string.isRequired,
  /**
   * Function call when all the answers are answered
   */
  onFinishGame: PropTypes.func.isRequired,
  /**
   * Function call when the CTA to go to the next game is clicked on
   */
  onNextGame: PropTypes.func.isRequired,
  /**
   * Block component of the current mini-game
   */
  Block: PropTypes.func.isRequired,
}

export default Game
