import React from 'react'

import Loader from '..'

export default {
  title: 'Loader',
  parameters: {
    component: Loader,
  },
}

export const main = () => <Loader />
