import React, { useState } from 'react'
import styled from 'styled-components'
import { ThemeProvider, StylesProvider } from '@material-ui/styles'
//import { IntlProvider } from 'react-intl'
import { GlobalStyle, themes } from '../../style'
import Typography from '@material-ui/core/Typography'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import SessionTable from '../SessionTable'
import SessionIpWhiteList from '../SessionIpWhitList'

const DashboardTabs = styled((props) => {
  return <Tabs {...props} classes={{ indicator: 'indicator' }}></Tabs>
})`
  margin: 20px 50px;
  border-bottom: 1px solid #bec6d1;
  && .indicator {
    background-color: #57bdbf;
  }
`
const DashboardTab = styled(Tab)`
  font-size: 1em;
  text-transform: none;
`

const TabPanel = styled(Typography)``

const TableWrapper = styled.div`
  margin: 50px;
`

// Get all language files
const context = require.context(
  '../../../app/config/languages/',
  false,
  /\.js$/
)
const filenameRegexp = /^\.\/(.*)\.js$/
const languageFiles = context.keys().reduce((acc, filename) => {
  return {
    ...acc,
    [filename.match(filenameRegexp)[1]]: context(filename).default,
  }
}, {})

const DashboardCompany = () => {
  // ! Need to refresh to change the language
  const lang = localStorage.getItem('preferredLanguage')
  const [tab, setTab] = useState(0)

  // TODO Bindings from angularJS

  const handleChange = (event, newValue) => {
    setTab(newValue)
  }

  /**
   * From https://material-ui.com/components/tabs/
   * @param {*} index
   */
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    }
  }

  return (
    // <IntlProvider locale={lang} messages={languageFiles[lang]}>
    <StylesProvider injectFirst>
      <GlobalStyle />
      <ThemeProvider theme={themes['goshaba']}>
        <DashboardTabs
          value={tab}
          onChange={handleChange}
          aria-label="dashbord nav"
        >
          <DashboardTab
            label={languageFiles[lang]['CANDIDATE_LIST']}
            {...a11yProps(0)}
          />
          <DashboardTab
            label={languageFiles[lang]['IP_WHITE_LIST']}
            {...a11yProps(1)}
          />
        </DashboardTabs>
        <TableWrapper>
          <TabPanel
            component="div"
            role="tabpanel"
            hidden={tab !== 0}
            id={`simple-tabpanel-candidates`}
            aria-labelledby={`simple-tab-candidates`}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 5 }}
          >
            <SessionTable lang={lang} />
          </TabPanel>

          <TabPanel
            component="div"
            role="tabpanel"
            hidden={tab !== 1}
            id={`simple-tabpanel-ip`}
            aria-labelledby={`simple-tab-ip`}
          >
            <SessionIpWhiteList lang={lang} />
          </TabPanel>
        </TableWrapper>
      </ThemeProvider>
    </StylesProvider>
    // </IntlProvider>
  )
}

DashboardCompany.propTypes = {}

export default DashboardCompany
